"""
WSGI config for pln project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import sys
import site
import os

# Munge the path for the virtualenv.
root = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
vepath = "/venv/pln/lib/python2.6/site-packages"

prev_sys_path = list(sys.path)
site.addsitedir(vepath)
sys.path.append(os.path.join(root, ".."))
sys.path.append(root)

# move all the recent path additions to the top
new_sys_path = [p for p in sys.path if p not in prev_sys_path]
for item in new_sys_path:
    sys.path.remove(item)
sys.path[:0] = new_sys_path


os.environ["DJANGO_SETTINGS_MODULE"] = "pln.settings"

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
