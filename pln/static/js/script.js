// console.log safety: shadow console.log if it doesn't exist so we don't break
// stupid browsers.
if (!(window.console && window.console.log && typeof(console.log) == 'function')) {
    window.console = { log: function() {} };
}

/* 
    Given a page with a search form which returns results on the same page,
    eliminate the page load, and make every keypress on that form trigger
    instant search.  Do this using pushState in the browser, and parsing the
    DOM of the full page returned (e.g. the server thinks you're just reloading
    the whole page). 
*/
function instasearch(form_selector, results_selector) {
    var valueCache = {};
    if (window.history.pushState) {
        var subTimeout = null;
        var sub = function() { $(form_selector).submit(); }
        $("select", form_selector).on('change', sub);
        $("input[type=text]", form_selector).not(".select2-input").not(".ui-autocomplete-input").on('keydown', function() {
            valueCache[$(this).attr("name")] = $(this).val();
        });
        $("input[type=text]", form_selector).not(".select2-input").not(".ui-autocomplete-input").on('keyup', function() {
            if (valueCache[$(this).attr("name")] != $(this).val()) {
                sub();
            }
            valueCache[$(this).attr("name")] = $(this).val();
        });
        $("input[type=checkbox], .select2-controlled", form_selector).on('change', sub);
        $(form_selector).submit(function(event) {
            event.preventDefault();
            $(".loading", results_selector).show();
            if (subTimeout) {
                clearTimeout(subTimeout);
            }
            subTimeout = setTimeout(function() {
                console.log($(form_selector).formSerialize());
                $.ajax({
                    url:"?" + $(form_selector).formSerialize(),
                    type: 'GET',
                    success: function(data) {
                        console.log("WHAT??");
                        var new_doc = $("<div>" + data + "</div>");
                        $(results_selector, document).html(
                            $(results_selector, new_doc).html()
                        );
                        history.replaceState({}, "", "?" + $(form_selector).formSerialize());
                        registerConfirmations($(results_selector));
                    },
                    error: function(data) {
                        console.error(data);
                    }
                });
            }, 500);
            return false;
        });
    }
}

/*
    Add a 'today' control after, and a 'interpretation' field below, a text
    input field which takes a date input.
*/
function dateinput(selector) {
    $(selector).each(function() {
            var dateinput = $(this);
            var freeinput = $("<input>").attr({
                'type': 'text',
                'value': dateinput.val().split("T")[0]
            });
            dateinput.after(freeinput);
            dateinput.hide();
            freeinput.on('change keyup', function() {
                if (freeinput.val() == "") {
                    feedback.html("");
                    $(dateinput).val("");
                } else {
                    var result = Date.parse(freeinput.val());
                    feedback.html(
                        result.toString('dddd MMMM d, yyyy')
                    );
                    $(dateinput).val(result.toString('yyyy-MM-dd'));
                }
            });
            var a = $("<a href='#'>").html("Today").on('click', function() {
                freeinput.val(Date.today().toString('yyyy-MM-dd'));
                freeinput.change();
                return false;
            });
            var feedback = $("<div class='date-feedback'>");
            freeinput.after(feedback);
            freeinput.after(a);
            freeinput.change();
    });
}

/*
* Confirmation modals. Prompt for confirmation before submitting form.
*/
var confirmForm = function() {
    if (confirm("Are you sure you want to delete this?  There is no undo.")) {
        return true;
    }
    return false;
}
var confirmClick = function() {
    if (confirm("Are you sure you want to delete this?  There is no undo.")) {
        $.ajax({
            url: $(this).attr("href"),
            type: 'post',
            success: function() {
                window.location.reload(true);
            },
            error: function() {
                alert("Server error...");
            }
        });
    }
    return false;
}
function registerConfirmations(scope) {
    $("form.confirm", scope).on("submit", confirmForm);
    $("a.confirm", scope).on("click", confirmClick);
    $("a.checkcount", scope).on("click", function() {
        var count = parseInt($(".paginator-count").html());
        if (!isNaN(count) && count > 10000) {
            return confirm("Are you sure? " + count + " is a large number of contacts, and will take a while. Do you want to contiue?");
        }
        return true;
    });
    $("input[type=submit].confirm", scope).on('click', function() {
        return confirm("Are you sure you want to delete this? There is no undo.");
    });
}
registerConfirmations(document);

/************************************************************************
* Specific element/page utilities
*/

// Decide whether to hide the advanced search form or not.
$("#contact_search").each(function() {
    var scope = $(this);
    var extra = $(".extra", scope);
    $(".toggle_advanced", scope).on('click', function() {
        extra.toggle(); 
        return false;
    });
    var now = new Date();
    var year = "" + now.getUTCFullYear();
    var month = "" + (now.getUTCMonth() + 1);
    month = month.length == 1 ? "0" + month : month;
    var day = "" + now.getUTCDate();
    day = day.length == 1 ? "0" + day : day;
    var defaultValues = {
        "has_been_censored": "1",
        "deceased": "3",
        "subject_of_litigation": "1",
        "monitoring": "1",
        "investigating": "1",
        "lost_contact": "3",
        "is_donor": "1",
        "currently_subscribing": "1",
        "sort": "last_name",
        "exclude_missing_names": true,
        "exclude_addressless": true,
        "with_perpetual_subscriptions": "1",
        "search_addresses_current_on_date": [year, month, day].join("-")
    }
    var checkDefault = function(el) {
        el = $(el);
        if (el.attr("type") == "submit") {
            return;
        }
        var expectedVal;
        var isExtra = $.contains(extra[0], el[0]);
        if (defaultValues[el.attr("name")] != null) {
            expectedVal = defaultValues[el.attr("name")];
        } else {
            expectedVal = "";
        }
        if (el.attr("type") == "checkbox") {
            if (el.is(":checked") != !!expectedVal) {
                if (isExtra) {
                    console.log(["showExtra:", el[0].id, !!expectedVal, el.is(":checked")]);
                    extra.show();
                }
                el.addClass("changed");
            } else {
                el.removeClass("changed");
            }
        } else if (el.attr("multiple")) {
            if (el.find("option:selected").length > 0) {
                console.log(["showExtra:", el[0].id, "multiple"]);
                extra.show();
            }
        } else {
            if (el.val() != expectedVal) {
                if (isExtra) {
                    console.log(["showExtra:", el[0].id, expectedVal, el.val()]);
                    extra.show();
                }
                el.addClass("changed");
            } else {
                el.removeClass("changed");
            }
            // Special-casing the select2 elements to highlight the controlled
            // value.
            if (el.hasClass("select2-controlled")) {
                el.prev().toggleClass("changed", el.val() != expectedVal);
            }
        }
    }
    $("input, select", scope).each(function(i, el) {
        checkDefault(el);
    });
    $("input, select", scope).change(function(event) {
        checkDefault(event.currentTarget);
    });
    $("#id_tag_search", scope).each(function(i, el) {
        var $el = $(el);
        var allTags = [];
        var checked = [];
        $el.find("option").each(function (i, opt) {
            var val = $(opt).html();
            allTags.push(val);
            if ($(opt).is(":selected")) {
                checked.push(val);
            }
        });
        var input = $("<input name='tagit-proxy' type='text' />").val(checked.join(","));
        $el.before(input);
        input.tagit({
            signleField: true,
            singleFieldDelimiter: ",",
            singleFieldNode: input,
            availableTags: allTags,
            allowSpaces: true,
            afterTagAdded: function(duringInitialization) {
                if (!duringInitialization) { input.change(); }
            },
            afterTagRemoved: function(duringInitialization) {
                if (!duringInitialization) { input.change(); }
            }
        })
        input.on("change", function() {
            var parts = input.val().split(",");
            $el.find("option").each(function(i, opt) {
                var val = $(opt).html();
                var found = false;
                for (var i = 0; i < parts.length; i++) {
                    if (val == parts[i]) {
                        found = true;
                        break;
                    }
                }
                $(opt).attr("selected", found);
            });
            $el.change()
        });
        $el.hide()
    });
});
// Convert id_prison forms to select2's.
$("form #id_prison[type=text], form .select-prison").each(function(i, el) {
    var state = $("<input name='prison-state' size=2 type='text' />");
    var city = $("<input name='prison-city' size=30 type='text' />");
    var query = $(el);
    query.addClass("select2-controlled");
    $(el).before([state, city]);

    function resultify(list) {
        var results = [];
        for (var i = 0; i < list.length; i++) {
            results.push({text: list[i], id: list[i]});
        }
        return results;
    }

    function updateResults($el, items) {
        var val = $el.val();
        $el.select2("results", items);
        $el.val(val).trigger("change");
    }

    state.select2({
        allowClear: true,
        placeholder: "State",
        width: "6em",
        ajax: {
            url: "/prisons/json",
            dataType: 'json',
            data: function (term) {
                console.log("!!!", term, city.val(), query.val())
                return {
                    state: term,
                    city: city.val(),
                    q: query.val()
                }
            },
            results: function (data) {
                //updateResults(city, resultify(data.cities));
                //updateResults(query, data.results);
                return { results: resultify(data.states) };
            }
        }
    }); 
    city.select2({
        allowClear: true,
        placeholder: "City",
        width: "10em",
        ajax: {
            url: "/prisons/json",
            dataType: "json",
            data: function (term) {
                return {
                    state: state.val(),
                    city: term,
                    q: query.val()
                };
            },
            results: function (data) {
                //updateResults(state, resultify(data.states));
                //updateResults(query, data.results);
                return { results: resultify(data.cities) };
            }
        }
    });
    query.select2({
        allowClear: true,
        placeholder: "Prison",
        width: "20em",
        ajax: {
            url: "/prisons/json",
            dataType: "json",
            data: function (term, page) {
                return {
                    q: term,
                    city: city.val(),
                    state: state.val(),
                    page: page,
                    per_page: 50
                };
            },
            results: function (data, page) {
                var more = (page * 50) < data.count;
                //updateResults(state, resultify(data.states))
                //updateResults(city, resultify(data.cities))
                return { results: data.results, more: more };
            }
        },
        initSelection: function (el, callback) {
            var id = $(el).val();
            if (id !== "") {
                $.ajax("/prisons/json?id=" + encodeURIComponent(id), {
                    dataType: "json"
                }).done(function(data) {
                    callback(data.results[0]);
                });
            }
        }
    });
});

// Utilities for ajax editing of notes.
var editNote = function() {
    var url = $(this).attr("data-edit-url");
    var note = $(this).parentsUntil(".note").parent();
    var text = note.find(".text").text();
    var form = note.parent().find("form");
    note.hide()
    form.show()
    form.attr("action", url);
    $("textarea", form).val(text);
    form[0].scrollIntoView();
}
$(".notes-section .edit-note").on("click", editNote);
var submitNoteForm = function(event) {
    var form = $(this);
    $("input[type=submit]", form).addClass("loading")
    $.ajax({
        url: form.attr("action"),
        type: 'POST',
        data: {
            text: $("textarea[name=text]", form).val()
        },
        success: function(data) {
            var doc = $(data);
            $(".notes-section").replaceWith(doc); 
            // Ugh... reload all event handlers for replaced dom.
            $(".confirm", doc).on("click", confirmClick);
            $(".edit-note", doc).on("click", editNote);
            $("form", doc).on("submit", submitNoteForm);
        },
        error: function(err) {
            alert("Server error!  Note not saved.");
            $("input[type=submit]", form).removeClass("loading")
            console.log(err);
        }
    });
    return false;
}
$(".edit-note-form").on("submit", submitNoteForm);

// Datepickers for certain date elements.
$("#id_search_addresses_current_on_date, .needs-datepicker").datepicker({
    minDate: new Date(1995, 1 - 1, 1),
    maxDate: new Date(),
    dateFormat: "yy-mm-dd"
}).attr({
    placeholder: "YYYY-MM-DD"
});
// tag lists
$("#id_tag_list").each(function(i, el) {
    var $el = $(el);
    var tagitEl = $("<ul></ul>");
    $el.before(tagitEl);
    var availableTags = [];
    var availableStr = $el.attr("data-available-tags");
    if (availableStr) {
        availableTags = JSON.parse(availableStr);
    }
    tagitEl.tagit({
        singleField: true,
        singleFieldDelimiter: ",",
        singleFieldNode: el,
        availableTags: availableTags,
        allowSpaces: true
    });
    $el.hide();
});
