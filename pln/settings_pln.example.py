from .default_settings import *

DEBUG = False

ADMINS = MANAGERS = (('Your Admin', 'youradmin@example.com'),)

ALLOWED_HOSTS = ['www.example.com', ]

SUBSCRIPTION_CUTOFF = "2010-01-01T00:00:00"

SUBSCRIBER_DATABASE_HEADER = "PLN Subscriber Database"
SUBSCRIBER_DATABASE_ABBREVIATION = "PLN"
SUBSCRIBER_DATABASE_STYLES = "css/style.pln.css"

SUBSCRIBER_DATABASE_SERVER_PORT = 8089
SUBSCRIBER_DATABASE_SERVER_HOST = "0.0.0.0"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pln',
        'USER': 'pln',
        'PASSWORD': 'todo',
        'HOST': '',
        'PORT': '',
    }
}

SESSION_COOKIE_NAME = 'pln_sessionid'
