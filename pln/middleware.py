from time import time
from logging import getLogger
from django.utils.deprecation import MiddlewareMixin

class LoggingMiddleware(MiddlewareMixin):
    def __init__(self, get_response=None):
        self.logger = getLogger('access_log')
        self.get_response = get_response

    def process_request(self, request):
        request.timer = time()
        return None

    def process_response(self, request, response):
        if hasattr(request, "user"):
            username = request.user.username
        else:
            username = "(unauthenticated)"
        if hasattr(request, "timer"):
            elapsed = time() - request.timer
        else:
            elapsed = -1
        self.logger.info(
            '%s %s [%s] %s (%.1fs)',
            request.META['REMOTE_ADDR'],
            username,
            response.status_code,
            request.get_full_path(),
            elapsed
        )
        return response
