from django.conf import settings


def subscriber_database_context(request):
    return {
        'subscriber_database_header': settings.SUBSCRIBER_DATABASE_HEADER,
        'subscriber_database_abbreviation': settings.SUBSCRIBER_DATABASE_ABBREVIATION,
        'subscriber_database_styles': settings.SUBSCRIBER_DATABASE_STYLES,
        'CSRF_COOKIE_NAME': settings.CSRF_COOKIE_NAME,
    }
