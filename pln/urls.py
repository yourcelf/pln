from django.conf.urls import include, url
from django.shortcuts import redirect
from django.contrib.auth import views as auth_views
from django.contrib import admin

from pln.admin import admin_site


# Method to fake a 500 error.
def bad(request): return 1 / 0

urlpatterns = [
    url(r'bad$', bad),

    url(r'^admin/', admin_site.urls),

    url(r'^accounts/login/$', auth_views.LoginView.as_view(template_name='login.html'),
        name='auth_login'),
    url(r'^accounts/logout/$', auth_views.LogoutView.as_view(template_name='logout.html'),
        name='auth_logout'),

    url(r'', include('subscribers.urls')),

    url(r'^$', lambda r: redirect('subscribers.contacts')),
]
