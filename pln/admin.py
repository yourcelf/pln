from django.contrib.admin import AdminSite
from django.contrib.auth.models import Group
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth import get_user_model
from django.conf import settings


class MyAdminSite(AdminSite):
    site_header = "{} Administration".format(settings.SUBSCRIBER_DATABASE_HEADER)
    site_title = "{} Admin".format(settings.SUBSCRIBER_DATABASE_HEADER)
    index_title = "{} Administration".format(settings.SUBSCRIBER_DATABASE_ABBREVIATION)

admin_site = MyAdminSite(name="myadmin")

admin_site.register(Group, GroupAdmin)
admin_site.register(get_user_model(), UserAdmin)
