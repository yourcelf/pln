#!/bin/bash

DBNAME=pln_dev
DBUSER=pln
DBPASS=pln
DBSUPER=postgres

# Drop existing db's / users
sudo su $DBSUPER -c "psql -c \"DROP DATABASE IF EXISTS $DBNAME;\""
sudo su $DBSUPER -c "psql -c \"DROP USER IF EXISTS $DBUSER;\""
# Create new user / db.
sudo su $DBSUPER -c "psql -c \"CREATE USER $DBUSER WITH CREATEDB PASSWORD '$DBPASS';\""
sudo su $DBSUPER -c "createdb $DBNAME -O $DBUSER"
# Load models, and add pg_trgm extension for fuzzy text search.
sudo su $DBSUPER -c "psql -d $DBNAME -c \"CREATE EXTENSION pg_trgm;\"" # Postgres 9.1
#sudo su $DBSUPER -c "psql -d $DBNAME < /usr/share/postgresql/8.4/contrib/pg_trgm.sql" # Postgres 8.4
python manage.py syncdb --noinput
sudo su $DBSUPER -c "psql -d $DBNAME -c \"CREATE INDEX trgm_idx ON subscribers_contact USING gin(first_name gin_trgm_ops);\""
sudo su $DBSUPER -c "psql -d $DBNAME -c \"CREATE INDEX trgm_idx ON subscribers_contact USING gin(middle_name gin_trgm_ops);\""
sudo su $DBSUPER -c "psql -d $DBNAME -c \"CREATE INDEX trgm_idx ON subscribers_contact USING gin(last_name gin_trgm_ops);\""
# Create django superuser
python manage.py shell <<-EOF
from django.contrib.auth.models import User
u = User.objects.create(username='admin', is_superuser=True, is_staff=True)
u.set_password('admin')
u.save()
exit()
EOF
python manage.py migrate
