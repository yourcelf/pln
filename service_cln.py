#!python

"""
This module runs a cherrypy server to host the Django app, and defines a win32
service to run it under Windows.  Run with:

    python service_cln.py

or under Windows, from this file's directory:

    service_cln.py
"""

import cherrypy
import os

import django.core.handlers.wsgi
from django.conf import settings

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
ERROR_LOG = os.path.join(BASE_DIR, "logs", "cln.error.log")


def run_server():
    os.chdir(BASE_DIR)

    os.environ['DJANGO_SETTINGS_MODULE'] = 'pln.settings_cln'
    django.setup()

    app = django.core.handlers.wsgi.WSGIHandler()

    # See:
    # http://stackoverflow.com/questions/9315787/flask-cherrypy-and-static-content
    # http://rhodesmill.org/brandon/2011/wsgi-under-cherrypy/
    cherrypy.tree.graft(app, '/')
    cherrypy.tree.mount(None, '/static', {'/': {
        'tools.staticdir.dir': settings.STATIC_ROOT,
        'tools.staticdir.on': True,
    }})
    cherrypy.config.update({
        'server.socket_port': settings.SUBSCRIBER_DATABASE_SERVER_PORT,
        'server.socket_host': settings.SUBSCRIBER_DATABASE_SERVER_HOST,
        'engine.autoreload.on': False,
        'log.error_file': ERROR_LOG,
        'log.screen': False,
        'tools.log_tracebacks.on': True,
    })
    cherrypy.engine.start()
    cherrypy.engine.block()

if os.name == "nt":
    import win32serviceutil
    import win32service

    # http://tools.cherrypy.org/wiki/WindowsService
    class Service(win32serviceutil.ServiceFramework):
        """NT Service."""

        _svc_name_ = "ClnDatabaseWebApp"
        _svc_display_name_ = "CLN Database Web App"

        def SvcDoRun(self):
            run_server()
            self.ReportServiceStatus(win32service.SERVICE_RUNNING)

        def SvcStop(self):
            self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
            cherrypy.engine.exit()
            self.ReportServiceStatus(win32service.SERVICE_STOPPED)

    if __name__ == "__main__":
        win32serviceutil.HandleCommandLine(Service)
else:
    if __name__ == "__main__":
        run_server()
