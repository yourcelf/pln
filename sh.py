#
# Useful things to import when using manage.py shell
#
# Usage:
#
# % python manage.py shell
# >>> from sh import *
# >>> # Now you have all these useful things at your disposal.
#
from pprint import pprint

from django.core.cache import cache
from django.contrib.auth.models import *
from django.db.models import *

from subscribers.models import *
