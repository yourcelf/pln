from django.conf.urls import include, url
import subscribers.views as v

urlpatterns = [
    #
    # Contact adding, displaying, and searching, editing
    #
    url(r'^contacts/$', v.contacts, name='subscribers.contacts'),
    url(r'^contacts/add$', v.add_contact, name='subscribers.contacts.add'),
    url(r'^contacts/(?P<contact_id>\d+)/$', v.show_contact, name='subscribers.contacts.show'),
    url(r'^contacts/inquiry/(?P<inquiry_id>\d+)/delete', v.delete_inquiry, name='subscribers.contacts.delete_inquiry'),
    url(r'^contacts/otherpurchase/(?P<other_purchase_id>\d+)/delete', v.delete_other_purchase, name='subscribers.contacts.delete_other_purchase'),
    url(r'^contacts/mailingstay/(?P<mailingstay_id>\d+)/end', v.end_mailing_stay, name='subscribers.contacts.end_mailing_stay'),
    url(r'^contacts/donors$', v.donor_batches, name='subscribers.contacts.donor_batches'),
    url(r'^contacts/donors/delete/(?P<donor_batch_id>\d+)$', v.delete_donor_batch, name='subscribers.contacts.delete_donor_batch'),
    url(r'^contacts/donors/count$', v.donor_batch_count, name='subscribers.contacts.donor_batch_count'),

    url(r'^notes/(?P<contact_id>\d+)/(?P<note_id>\d+)?$', v.list_notes, name='subscribers.list_notes'),
    url(r'^notes/delete/(?P<note_id>\d+)?$', v.delete_note, name='subscribers.delete_note'),
    url(r'^contacts/fuzzy$', v.fuzzy_contact_search, name='subscribers.contacts.fuzzy_search'),
    url(r'^contactaddress/(?P<contactaddress_id>\d+)/delete$', v.delete_contactaddress, name='subscribers.contactaddress.delete'),
    url(r'^contacts/import/$', v.import_subscriber_spreadsheet, name='subscribers.import_subscriber_spreadsheet'),

    #
    # JSON searches
    #
    url(r'^contacts/json$', v.json_contact_search, name='subscribers.contacts.json'),
    url(r'^prisons/json$', v.json_prison_search, name='subscribers.prisons.json'),

    #
    # Subscriptions
    #
    url(r'^subscription/(?P<subscription_id>\d+)/delete$', v.delete_subscription, name='subscribers.subscription.delete'),
    url(r'^subscription/(?P<subscription_id>\d+)/end_early$', v.end_subscription_early, name='subscribers.subscription.end_early'),
    url(r'^subscription/(?P<subscription_id>\d+)/restart$', v.restart_subscription, name='subscribers.subscription.restart'),

    #
    # Mailings
    #
    url(r'^mark_censored/(?P<mailing_id>\d+)$', v.mark_censored, name='subscribers.mark_censored'),

    url(r'^mailings/(?P<mailing_id>\d+)/mark_sent', v.mark_mailing_sent, name='subscribers.mailings.mark_sent'),
    url(r'^mailings/(?P<mailing_id>\d+)/delete_mailing', v.delete_mailing, name='subscribers.mailings.delete_mailing'),
    url(r'^mailings/log/$', v.mailings_sent, name='subscribers.mailinglogs.list'),
    url(r'^mailings/log/(?P<mailinglog_id>\d+)/show', v.show_mailinglog, name='subscribers.mailinglogs.show'),
    url(r'^mailings/log/(?P<mailinglog_id>\d+)/delete$', v.delete_mailinglog, name='subscribers.mailinglogs.delete'),
    url(r'^mailings/needed/(?P<issue_id>\d+)?$', v.mailings_needed, name='subscribers.mailings.needed'),
    url(r'^mailings/institution_labels$', v.institution_labels, name='subscribers.mailings.institution_labels'),
    url(r'^mailings/institution_spreadsheet$', v.institution_spreadsheet, name='subscribers.mailings.institution_spreadsheet'),

    #
    # Issues
    #
    url(r'^issues/(?P<issue_id>\d+)$', v.show_issue, name='subscribers.issues.show'),
    url(r'^issues/list$', v.list_issues, name='subscribers.issues.list'),
    url(r'^issues/(?P<issue_id>\d+)/delete$', v.delete_issue, name='subscribers.issues.delete'),

    #
    # Stats
    #
    url(r'^stats/$', v.stats, name='subscribers.stats'),
    #url(r'^stats/map$', v.stats_map, name='subscribers.stats.map'), # disabled for now..
    #url(r'^stats/subscriptions$', v.stats_subscriptions, name='subscribers.stats.subscriptions'),
    url(r'^stats/questions$', v.stats_questions, name='subscribers.stats.questions'),
    url(r'^stats/answers$', v.stats_answers, name='subscribers.stats.answers'),

    #
    # Sharing
    #
    url(r'^sharing/$', v.sharing, name='subscribers.sharing'),
    url(r'^sharingbatch/(?P<sharingbatch_id>\d+)/delete', v.delete_sharingbatch, name='subscribers.sharingbatch.delete'),
    url(r'^sharingbatch/(?P<sharingbatch_id>\d+)/(?P<filetype>\w+)', v.download_sharingbatch, name='subscribers.sharingbatch.download'),
    url(r'^sharingbatch/count', v.sharingbatch_count, name='subscribers.sharingbatch.count'),

    #
    # Management
    #
    url(r"^upgrade/$", v.upgrade, name='upgrade'),
    url(r"^restart/$", v.restart, name='restart'),
    url(r"^restart/do$", v.do_restart, name='restart_do'),
]
