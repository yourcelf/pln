"""
This module defines actions for what to happen when a new Inquiry is logged.
"""
from django.utils.timezone import now
from subscribers.models import *
from django.urls import reverse

def handle(inquiry):
    desc = inquiry.response_type.description
    if desc == "Stay the subscription":
        return add_stay(inquiry)
    if desc == 'Cancel':
        return cancel_subscription(inquiry)
    if desc == 'Unstay the subscription':
        return remove_stay(inquiry)
    if desc == 'Mark as deceased':
        return mark_deceased(inquiry)
#    elif desc == 'Confirmation letter':
#        pass
#    elif desc == 'Censorship letter':
#        pass
    if desc == 'Blacklist':
        return cancel_subscription(inquiry, "Blacklisted")
#    elif desc == "Can't help":
#        pass
#    elif desc == 'None':
#        pass
#    elif desc == 'Censorship card':
#        pass
#    elif desc == 'Personal':
#        pass
#    elif desc == 'Pro-rated':
#        pass
#    elif desc == 'Check with mailroom':
#        pass
#    elif desc == '4-6 weeks':
#        pass
#    elif desc == 'Issue not yet mailed':
#        pass
#    elif desc == 'Address incorrect':
#        pass
#    elif desc == 'COA not in time':
#        pass
#    elif desc == 'Renewal not in time':
#        pass
#    elif desc == 'Funds not received':
#        pass
    if inquiry.response_type.generate_mailing:
        return Mailing.objects.create(
            type=MailingType.objects.get(type='Inquiry'),
            inquiry=inquiry,
            contact=inquiry.contact,
        )

def add_stay(inquiry):
    contact = inquiry.contact
    try:
        contact.mailingstay_set.stayed().get()
    except MailingStay.DoesNotExist:
        contact.mailingstay_set.create()
    return None

def remove_stay(inquiry):
    contact = inquiry.contact
    contact.mailingstay_set.stayed().update(end_date=now())
    return None

def mark_deceased(inquiry):
    contact = inquiry.contact
    contact.deceased = True
    contact.save()
    cancel_subscription(inquiry, "Deceased")

def cancel_subscription(inquiry, reason_description=None):
    desc = inquiry.request_type.description
    stop_reasons = {
        "Mail Returned Deceased": "Deceased",
        "Censorship": "Mail Returned",
        #"Unspecified": "",
        #"Book Not Received": "",
        #"Other": "",
        #"Didn't Order": "",
        #"Dislikes Content": "",
        #"No Time to Read": "",
        #"Missing Issues": "",
        #"No Issues Yet": "",
        #"Sample Not Received": "",
        #"Back Issue Not Received": "",
        #"Moving Destination Unknown": "",
        #"Unstay Subscription": "",
        #"Unexpected Expiration Date": "",
        #"Being Released": "In Transit",
        #"Request for Help": "",
    }
    stop_reason = SubscriptionStopReason.objects.get(
            reason=reason_description or stop_reasons.get(desc, "Cancelled")
    )
    subs = list(inquiry.contact.subscription_set.exclude(
                number_of_issues=0
            ).order_by('-start_date'))
    contact = inquiry.contact
    while contact.issues_remaining != 0 and len(subs) > 0:
        subs.pop(0).end_early(stop_reason)
        contact = Contact.objects.get(pk=contact.pk)
