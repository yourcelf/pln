from collections import defaultdict
from django.utils.timezone import now

from subscribers.models import *

"""
This module provides a set of classes that help to manage the generation and
display of outgoing mail, following a very basic state transition model.

Mail is in one of 4 states:
    - needed: The rules / business logic say it should exist, but it doesn't
      yet.
    - enqueued: A ``Mailing`` model has been created for this mail requirement.
    - sent: A ``Mailing`` object exists, and it has been marked sent.
    - unreachable: The contact can't be reached for whatever reason --
      deceased, lost contact, subscription stayed, etc.

The purpose of a MailingRule class is to define a queryset for the number of
mailings needed, and provide a transition between the above states.  The basic
properties of each class are:

    - qs: The queryset that defines all needed mailings.
    - state: one of ("needed", "enqueued", "sent", "unreachable")
    - transition: a method to this class's mailings from "needed" to "enqueued"
      or "enqueued" to "sent".
    - code: A unique identifier usable in URL querystrings to refer to this
      mailing.

Based on that principle, we define mailing rules for:

    Issues
    Notices (last issue warnings, etc)
    Inquiry Responses

"""

class MailingRule(object):
    # Must be provided by subclass:
    qs = None
    mtype = None
    name = None
    state = None
    code = None
    # Optional:
    action = None
    issue = None
    inquiry = None

    def __init__(self, *args, **kwargs):
        self._count = None
        self.when = kwargs.pop('when', now())
        super(MailingRule, self).__init__()

    def count(self):
        if self._count is None:
            self._count = self.qs.count()
        return self._count

    def transition(self, user):
        pass

class ContactMailingNeeded(MailingRule):
    """
    Base class for a mailing that comes from a queryset of Contacts.
    """
    action = "Prepare"
    state = "needed"

    def __init__(self, *args, **kwargs):
        super(ContactMailingNeeded, self).__init__(*args, **kwargs)

    def transition(self, user):
        """
        NOTE: This doesn't ever call ``Mailing.save``, so cubclasses that
        need the ``self.contact.set_issues_remaining()`` side effect of that
        need to handle that themselves.
        """
        existing_contact_ids = set(Mailing.objects.filter(
            issue=self.issue, inquiry=self.inquiry, type=self.mtype
        ).values_list('contact_id', flat=True))
        desired_contact_ids = set(self.qs.values_list('id', flat=True))

        to_create = desired_contact_ids - existing_contact_ids
        mailings = []
        for contact_id in desired_contact_ids:
            mailings.append(Mailing(
                type=self.mtype,
                issue=self.issue,
                inquiry=self.inquiry,
                contact_id=contact_id))
        Mailing.objects.bulk_create(mailings)

    def __str__(self):
        return "Contacts needing {0}".format(self.name)

class ContactMailingEnqueued(MailingRule):
    note = ""
    action = "Send"
    state = "enqueued"
    def transition(self, user):
        """
        NOTE: This doesn't ever call ``Mailing.save``, so cubclasses that
        need the ``self.contact.set_issues_remaining()`` side effect of that
        need to handle that themselves.
        """
        log = MailingLog.objects.create(
                user=user,
                type=self.mtype,
                note=self.note,
        )
        mailings = []
        contact_ids = list(self.qs.values_list('id', flat=True))
        for mailing in Mailing.objects.filter(
                contact__id__in=contact_ids,
                issue=self.issue,
                inquiry=self.inquiry,
                type=self.mtype,
                mailinglog__isnull=True,
                sent__isnull=True):
            mailings.append(mailing)

        if len(mailings) == 0:
            log.delete()
        else:
            log.mailings.set(mailings)
            log.mailings.all().update(sent=self.when)

    def __str__(self):
        return "Contacts with {0} enqueued".format(self.name)

class ContactMailingSent(MailingRule):
    action = None
    state = "sent"

    def __str__(self):
        return "Contacts who've been sent {0}".format(self.name)

    def transition(self, user):
        pass

class ContactMailingUnreachable(MailingRule):
    action = None
    state = "unreachable"
    def __str__(self):
        return "Unreachable contacts who need {0}".format(self.name)
    def transition(self, user):
        pass

#
# Issues
#

class IssueMailingBase(object):
    def __init__(self, *args, **kwargs):
        super(IssueMailingBase, self).__init__(*args, **kwargs)
        self.mtype = MailingType.objects.get(type="Issue")
        self.issue = kwargs['issue']
        self.name = str(self.issue)
        method = getattr(Contact.objects, "issue_{0}".format(self.state))
        self.qs = method(self.issue)
        self.code = "issue:{0}:{1}".format(self.issue.pk, self.state)

class IssuesNeeded(IssueMailingBase, ContactMailingNeeded):
    state = "needed"
    def __init__(self, *args, **kwargs):
        super(IssuesNeeded, self).__init__(*args, **kwargs)

    def transition(self, user):
        # Issues needed must be enqueued with multiple copies.
        mailings = []
        contacts = []
        for contact in self.qs:
            contacts.append(contact)
            if contact.issues_remaining == -1:
                copies = contact.copies
            else:
                copies = min(contact.copies, contact.issues_remaining)
            for i in range(0, copies):
                mailings.append(Mailing(
                    type=self.mtype,
                    issue=self.issue,
                    inquiry=self.inquiry,
                    contact=contact))
        Mailing.objects.bulk_create(mailings)

class IssuesUnreachable(IssueMailingBase, ContactMailingUnreachable):
    state = "unreachable"

class IssuesEnqueued(IssueMailingBase, ContactMailingEnqueued):
    state = "enqueued"
    def transition(self, user):
        # After sending issues, we need to update the number of issues
        # remaining for each contact.

        # NOTE: this is an optimization of: for contact in self.qs:
        # contact.set_issues_remaining() but must run *before* we transition
        # the mailing, otherwise self.qs is empty.  We do this so that the
        # parent class can use bulk methods to create Mailing objects rather
        # than calling ``save`` on them individually which is mighty slow.

        # Edge case: fewer than 'copies' issues remaining
        self.qs.filter(issues_remaining__gt=0, issues_remaining__lt=F('copies')).update(
            issues_remaining=0
        )
        # Main case: issues more than or same as number of copies.
        self.qs.filter(issues_remaining__gte=F('copies')).update(
            issues_remaining=F('issues_remaining')-F('copies')
        )
        super(IssuesEnqueued, self).transition(user)

class IssuesSent(IssueMailingBase, ContactMailingSent):
    state = "sent"

#
# Notices
#

class NoticeMailingBase(object):
    state = "unreachable"
    def __init__(self, months_remaining, rel, *args, **kwargs):
        super(NoticeMailingBase, self).__init__(*args, **kwargs)
        self.mtype = MailingType.objects.get(
                type=EXPIRATION_NOTICE_TYPES[months_remaining]
        )
        self.issue = kwargs.get('issue', None)
        self.name = "{0} ({1})".format(self.mtype, rel)
        self.note = "({0})".format(rel)
        method = getattr(Contact.objects, "notice_{0}".format(self.state))
        self.qs = method(months_remaining, rel, when=kwargs.get('when'))
        self.code = "notice:{0}:{1}:{2}".format(months_remaining, rel, self.state)

class NoticeNeeded(NoticeMailingBase, ContactMailingNeeded):
    state = "needed"
class NoticeUnreachable(NoticeMailingBase, ContactMailingUnreachable):
    state = "unreachable"
class NoticeEnqueued(NoticeMailingBase, ContactMailingEnqueued):
    state = "enqueued"
    def transition(self, user):
        type_buckets = [
            ("Individuals and Friends of PLN", ["individual", "fop"]),
            ("Prisoners", ["prisoner"]),
            ("Entities and Advertisers", ["entity", "advertiser"]),
        ]
        orig_qs = self.qs
        orig_note = self.note
        for note, typelist in type_buckets:
            self.note = " ".join([orig_note, note])
            self.qs = orig_qs.filter(type__in=typelist)
            super(NoticeEnqueued, self).transition(user)
        self.qs = orig_qs
        self.note = orig_note

class NoticeSent(NoticeMailingBase, ContactMailingSent):
    state = "sent"

#
# Inquiries
#

# Inquiries don't use "Contact" as the base type for querying, because a single
# contact could have multiple inquiries enqueued (unlike issues or resubscribe
# notices, which are 1-1 to contacts).  So we build the querysets on Inquiries
# instead.

class InquiryResponseBase(MailingRule):
    def __init__(self, *args, **kwargs):
        super(InquiryResponseBase, self).__init__(*args, **kwargs)
        self.mtype = MailingType.objects.get(type="Inquiry")
        self.name = "Inquiry Response"
        method = getattr(Inquiry.objects, "mailing_{0}".format(self.state))
        self.qs = method()
        self.code = "inquiry:{0}".format(self.state)

    def __str__(self):
        return "{0} inquiry responses".format(self.state.title())

class InquiryResponseNeeded(InquiryResponseBase):
    action = "Prepare"
    state = "needed"
    def transition(self, user):
        for inquiry in self.qs:
            Mailing.objects.get_or_create(
                    type=self.mtype,
                    issue=self.issue,
                    inquiry=inquiry,
                    contact=inquiry.contact,
            )

class InquiryResponseEnqueued(InquiryResponseBase):
    action = "Send"
    state = "enqueued"
    def transition(self, user):
        log = MailingLog.objects.create(
                user=user,
                type=self.mtype,
        )
        mailings = []
        for inquiry in self.qs:
            mailings.append(Mailing.objects.get(
                contact=inquiry.contact,
                inquiry=inquiry,
                type=self.mtype,
                mailinglog__isnull=True,
                sent__isnull=True))
        if len(mailings) == 0:
            log.delete()
        else:
            log.mailings.set(mailings)
            log.mailings.all().update(sent=self.when)

class InquiryResponseUnreachable(InquiryResponseBase):
    action = None
    state = "unreachable"

class InquiryResponseSent(InquiryResponseBase):
    action = None
    state = "sent"

#
# Other enqueued mailings
#
class EmptyRule(MailingRule):
    def __init__(self, *args, **kwargs):
        super(EmptyRule, self).__init__(*args, **kwargs)
        self.qs = Contact.objects.none()

class OtherEnqueuedMail(MailingRule):
    action = "Send"
    state = "enqueued"
    code = "other:enqueued"
    def __init__(self, current_issue, *args, **kwargs):
        """
        Current issue is used only to exclude mailing types that are current
        issues.
        """
        super(OtherEnqueuedMail, self).__init__(*args, **kwargs)
        self.mailing_qs = Mailing.objects.filter(
            ~Q(
                Q(type__type='Expired') |
                Q(type__type='Last Chance') |
                Q(type__type='Early Renew') |
                Q(type__type='Inquiry')
            ),
            sent__isnull=True,
        ).exclude(
            issue=current_issue,
            type__type='Issue',
        )
        self.qs = Contact.objects.filter(Contact.objects.reachable().q,
            mailing__id__in=self.mailing_qs.values_list('id', flat=True)
        )

    def transition(self, user):
        types = defaultdict(list)
        for mailing in self.mailing_qs.select_related('type'):
            types[mailing.type.type].append(mailing)
        for mtype_type, mailings in types.items():
            mtype = MailingType.objects.get(type=mtype_type)
            if len(mailings) > 0:
                log = MailingLog.objects.create(type=mtype, user=user)
                log.mailings.set(mailings)
                log.mailings.update(sent=self.when)

    def __str__(self):
        return "Other enqueued mail"
