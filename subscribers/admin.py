from django.contrib import admin
from pln.admin import admin_site

from subscribers.models import *

class PrisonAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'subject_of_litigation', 'monitoring', 'investigating', 'address']
    list_filter = ['type', 'subject_of_litigation', 'monitoring', 'investigating', 'admin_type', 'address__state']
    search_fields = ['name', 'type__name', 'address__address1', 'address__address2', 'address__region', 'address__state', 'address__zip', 'address__country']
    raw_id_fields = ['address', 'admin_address']
admin_site.register(Prison, PrisonAdmin)

class ContactAddressInline(admin.TabularInline):
    model = ContactAddress
    extra = 0

class SubscriptionInline(admin.TabularInline):
    model = Subscription
    fk_name = 'contact'
    extra = 0

class ContactAdmin(admin.ModelAdmin):
    #inlines = [ ContactAddressInline, SubscriptionInline, ]
    list_display = ['show_name', 'current_address', 'issues_remaining']
    search_fields = ['first_name', 'middle_name', 'last_name',
            'organization_name',
            'contactaddress__address__address1',
            'contactaddress__address__address2',
            'contactaddress__address__region',
            'contactaddress__address__city',
            'contactaddress__address__state',
            'contactaddress__address__zip']
    list_filter = ['gender', 'contactaddress__address__state']
    date_hierarchy = 'created'

admin_site.register(Contact, ContactAdmin)

class ContactAddressAdmin(admin.ModelAdmin):
    list_display = ['contact_name', 'prisoner_number', 'prison',
                    'unit', 'start_date', 'end_date']
    date_hierarchy = 'start_date'
    list_filter = ['prison__type', 'address__state',
                   'death_row', 'control_unit']
    raw_id_fields = ['contact', 'address', 'prison']
admin_site.register(ContactAddress, ContactAddressAdmin)

class IssueAdmin(admin.ModelAdmin):
    list_display = ['date', 'number', 'num_recipients', 'num_censored_recipients']
    date_hierarchy = 'date'
admin_site.register(Issue, IssueAdmin)

class MailingAdmin(admin.ModelAdmin):
    list_display = ['type', 'created', 'sent', 'contact_name', 'issue']
    list_filter = ['type']
    raw_id_fields = ['contact', 'inquiry']
admin_site.register(Mailing, MailingAdmin)

class MailingLogAdmin(admin.ModelAdmin):
    list_display = ['type', 'created', 'user']
    raw_id_fields = ['user']
    readonly_fields = ['mailings']
admin_site.register(MailingLog, MailingLogAdmin)

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['contact_name', 'start_date', 'number_of_issues', 'type']
    list_filter = ['type']
    date_hierarchy = 'start_date'
    raw_id_fields = ['contact', 'payer']
admin_site.register(Subscription, SubscriptionAdmin)

class AddressAdmin(admin.ModelAdmin):
    list_display = ['address1', 'address2', 'city', 'state', 'zip', 'region', 'country']
    list_filter = ['state']
    search_fields = ['address1', 'address2', 'city', 'state', 'zip', 'region', 'country']
admin_site.register(Address, AddressAdmin)

class NoteAdmin(admin.ModelAdmin):
    list_display = ['contact_name', 'author', 'text', 'created']
    list_filter = ['author']
    search_fields = ['contact__first_name',
            'contact__middle_name',
            'contact__last_name',
            'contact__organization_name',
            'text']
    date_hierarchy = 'created'
    raw_id_fields = ['contact', 'author']
admin_site.register(Note, NoteAdmin)

class InquiryResponseTypeAdmin(admin.ModelAdmin):
    list_display = ['description', 'code', 'generate_mailing']
admin_site.register(InquiryResponseType, InquiryResponseTypeAdmin)

class MailingStayAdmin(admin.ModelAdmin):
    list_display = ['contact', 'start_date', 'end_date']
    raw_id_fields = ['contact']
admin_site.register(MailingStay, MailingStayAdmin)

class InquiryAdmin(admin.ModelAdmin):
    list_display = ['contact', 'request_type', 'response_type', 'date']
    list_filter = ['request_type', 'response_type']
    raw_id_fields = ['contact']
    date_hierarchy = 'date'
admin_site.register(Inquiry, InquiryAdmin)

class SharingBatchAdmin(admin.ModelAdmin):
    list_display = ['partner', 'date']
    list_filter = ['partner']
    readonly_fields = ['contacts']
    date_hierarchy = 'date'
admin_site.register(SharingBatch, SharingBatchAdmin)

class OtherPurchaseAdmin(admin.ModelAdmin):
    list_display = ['contact', 'type', 'count', 'total_cost', 'date']
    date_hierarchy = 'date'
    raw_id_fields = ['contact']
    list_filter = ['type', 'count']
admin_site.register(OtherPurchase, OtherPurchaseAdmin)

class DonorBatchAdmin(admin.ModelAdmin):
    list_display = ['date']
    read_only_fields = ['contacts']
admin_site.register(DonorBatch, DonorBatchAdmin)

admin_site.register(PrisonAdminType)
admin_site.register(SharingPartner)
admin_site.register(InquiryType)
admin_site.register(OrganizationType)
admin_site.register(ContactSource)
admin_site.register(SubscriptionSource)
admin_site.register(SubscriptionStopReason)
admin_site.register(MailingType)
admin_site.register(OtherPurchaseType)
admin_site.register(Tag)
