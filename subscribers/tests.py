from collections import Counter
from datetime import timedelta
from django.test import TestCase
from django.utils.timezone import now, utc
from django.test.utils import override_settings
from django.core.management import call_command
from subscribers.models import *
from subscribers import mail_rules

class TestAddressLines(TestCase):
    def test_plain_individual(self):
        c = Contact.objects.create(first_name="First", last_name="Last")
        c.contactaddress_set.create(
            address=Address.objects.create(
                address1="123 Cherry Tree Lane",
                city="Oklahoma City",
                state="OK",
                zip="12345",
            )
        )
        self.assertEqual(c.current_address().get_address_lines(), [
            "First Last",
            "123 Cherry Tree Lane",
            "Oklahoma City, OK  12345",
        ])
                    

    def test_org(self):
        c = Contact.objects.create(
            first_name="First", last_name="Last", organization_name="Org"
        )
        c.contactaddress_set.create(
            address=Address.objects.create(
                address1="123 Cherry Tree Lane",
                city="Oklahoma City",
                state="OK",
                zip="12345",
            )
        )
        self.assertEqual(c.current_address().get_address_lines(), [
            "First Last",
            "Org",
            "123 Cherry Tree Lane",
            "Oklahoma City, OK  12345",
        ])

    def test_org_no_in_care_of(self):
        c = Contact.objects.create(
            organization_name="Org"
        )
        c.contactaddress_set.create(
            address=Address.objects.create(
                address1="123 Cherry Tree Lane",
                city="Oklahoma City",
                state="OK",
                zip="12345",
            )
        )
        self.assertEqual(c.current_address().get_address_lines(), [
            "Org",
            "123 Cherry Tree Lane",
            "Oklahoma City, OK  12345",
        ])

    def test_prison_address(self):
        c = Contact.objects.create(
            first_name="First", last_name="Last"
        )
        address = Address.objects.create(
            address1="123 Prison Lane",
            city="Oklahoma City",
            state="OK",
            zip="12345",
        )
        c.contactaddress_set.create(
            address=address,
            prison=Prison.objects.create(
                name="Big House",
                address=address,
            ),
            prisoner_number="103941",
            unit="Unit 5c",
        )
        self.assertEqual(c.current_address().get_address_lines(), [
            "First Last, #103941",
            "Unit 5c",
            "123 Prison Lane",
            "Oklahoma City, OK  12345",
        ])

    def test_international_address(self):
        c = Contact.objects.create(
            first_name="First", last_name="Last"
        )
        c.contactaddress_set.create(
            address = Address.objects.create(
                address1="123 Rue Pomme",
                city="Oklahoma City",
                region="Champlaine",
                zip="183-4897",
                country="France",
            )
        )
        self.assertEqual(c.current_address().get_address_lines(), [
            "First Last",
            "123 Rue Pomme",
            "Oklahoma City, Champlaine  183-4897",
            "France"
        ])


#
# Base classes
#

class SubscriberFactoryMethods(object):
    fixtures = ["initial_data.json"]
    def _add_issues(self):
        start = now()
        count = 15 # Number of months' issues before now to create
        self.issues = []
        for i in range(0, count):
            year = start.year
            month = start.month - i
            if month < 1:
                month += 12
                year -= 1
            date = datetime.datetime(year, month, 1, 0, 0, 0, 0, utc)
            self.issues.append(Issue.objects.create(
                volume=0, number=count - i, date=date
            ))
        self.issues.reverse()

    def _new_contact(self, name=None, **kwargs):
        if name:
            (first_name, last_name) = name.split(" ")
            kwargs['first_name'], kwargs['last_name'] = (first_name, last_name)
        defaults = dict(
            first_name="John",
            last_name="Dough",
            organization_name="",
            organization_type=None,
            email="john@dough.com",
            gender="M",
            type="prisoner",
            source=ContactSource.objects.get(category__name="Other", name="Unknown"),
            opt_out=False,
            created=now(),
            deceased=False,
            lost_contact=False,
            donor=False,
        )
        defaults.update(kwargs)
        contact = Contact.objects.create(**defaults)
        ContactAddress.objects.create(
            contact=contact,
            prison=Prison.objects.create(
                name="Test Prison",
                type=PrisonType.objects.get(name="Jail"),
                address=Address.objects.create(
                    address1="That Place",
                    city="City Town",
                    state="UT",
                    zip="84103",
                ),
            ),
            prisoner_number="".join(str(a) for a in range(len(name or "fun"))),
        )
        return Contact.objects.get(pk=contact.pk)

    def _new_subscription(self, contact, start_offset=0, end_offset=0, payer=None):
        if start_offset < 0:
            for i in range(start_offset - 1, min(end_offset or 0, 0)):
                for j in range(contact.copies):
                    Mailing.objects.create(
                        issue=self.issues[i],
                        type=MailingType.objects.get(type='Issue'),
                        contact=contact,
                        sent=self.issues[i].date,
                    )

        if end_offset is None:
            number_of_issues = -1
        else:
            number_of_issues = (end_offset - start_offset + 1) * contact.copies
        sub = Subscription.objects.create(
            contact=contact,
            number_of_issues=number_of_issues,
            start_date=self.issues[start_offset].date - datetime.timedelta(days=1),
            type='subscription',
            payer=payer or contact,
        )

        return sub

    def _new_mail_stay(self, contact, start_offset=0, end_offset=0):
        if end_offset:
            end_date = now() + datetime.timedelta(days=(start_offset or 0) * 31)
        else:
            end_date = None
        MailingStay.objects.create(
            contact=contact,
            start_date=now() + datetime.timedelta(days=start_offset * 31),
            end_date=end_date,
        )

@override_settings(SUBSCRIPTION_CUTOFF='1970-01-01T00:00:00Z')
class SubscriberFixtureTestCase(SubscriberFactoryMethods, TestCase):
    def setUp(self):
        self._add_issues()
        c = dict(
            nosub=self._new_contact(name="No Subscription"),

            # Self-paying subscribers of various types
            lapsed_self=self._new_contact(name="Lapsed Self"),
            perpetual_self=self._new_contact(name="Perpetual Self"),
            zero_left_self=self._new_contact(name="Zero Self"),
            one_left_self=self._new_contact(name="One Self"),
            two_left_self=self._new_contact(name="Two Self"),
            three_left_self=self._new_contact(name="Three Self"),

            # Recipient subscribers (not self-paying)
            lapsed_recipient=self._new_contact(name="Lapsed Recipient"),
            perpetual_recipient=self._new_contact(name="Perpetual Recipient"),
            zero_left_recipient=self._new_contact(name="Zero Recipient"),
            one_left_recipient=self._new_contact(name="One Recipient"),
            two_left_recipient=self._new_contact(name="Two Recipient"),
            three_left_recipient=self._new_contact(name="Three Recipient"),

            # Payers of subscribers (not receiving)
            lapsed_payer=self._new_contact(name="Lapsed Payer"),
            perpetual_payer=self._new_contact(name="Perpetual Payer"),
            zero_left_payer=self._new_contact(name="Zero Payer"),
            one_left_payer=self._new_contact(name="One Payer"),
            two_left_payer=self._new_contact(name="Two Payer"),
            three_left_payer=self._new_contact(name="Three Payer"),

            # Mailing stays, self paying
            stayed_self=self._new_contact(name="Stayed Self"),
            unstayed_self=self._new_contact(name="Unstayed Self"),
            deceased_self=self._new_contact(name="Deceased Self", deceased=True),
            lost_contact_self=self._new_contact(name="Lostcontact Self", lost_contact=True),

            # Mailing stays, receiving (not self paying)
            stayed_recipient=self._new_contact(name="Stayed Recipient"),
            unstayed_recipient=self._new_contact(name="Unstayed Recipient"),
            deceased_recipient=self._new_contact(name="Deceased Self", deceased=True),
            lost_contact_recipient=self._new_contact(name="Lostcontact Self", lost_contact=True),

            # Mailing stays, paying other (not receiving)
            stayed_payer=self._new_contact(name="Stayed Payer"),
            unstayed_payer=self._new_contact(name="UnStayed Payer"),
            deceased_payer=self._new_contact(name="Is Payer"),
            lost_contact_payer=self._new_contact(name="Lostcontact Payer"),

            # Payer with various ugly statuses.
            payer_dead_recipient=self._new_contact(name="PayerDead Recipient"),
            payer_dead=self._new_contact(name="Payer Dead", deceased=True),
            payer_lost_recipient=self._new_contact(name="PayerLost Recipient"),
            payer_lost=self._new_contact(name="Payer Lost", lost_contact=True),
            payer_stayed_recipient=self._new_contact(name="PayerStayed Recipient"),
            payer_stayed=self._new_contact(name="Payer Stayed"),
            payer_unstayed_recipient=self._new_contact(name="PayerUnstayed Recipient"),
            payer_unstayed=self._new_contact(name="Payer Unstayed"),

            # multiple copies
            multiple_copies_zero_left=self._new_contact(name="Multiple Zero", copies=2),
            multiple_copies_one_left=self._new_contact(name="Multiple One", copies=2),
            multiple_copies_two_left=self._new_contact(name="Multiple Two", copies=2),
            multiple_copies_lapsed=self._new_contact(name="Multiple Lapsed", copies=2),
        )

        self._new_subscription(c['lapsed_self'], -14, -1)
        self._new_subscription(c['lapsed_recipient'], -14, -1, c['lapsed_payer'])
        self._new_subscription(c['perpetual_self'], -1, None)
        self._new_subscription(c['perpetual_recipient'], -1, None, c['perpetual_payer'])
        self._new_subscription(c['zero_left_self'], -3, 0)
        self._new_subscription(c['zero_left_recipient'], -3, 0, c['zero_left_payer'])
        self._new_subscription(c['one_left_self'], -3, 1)
        self._new_subscription(c['one_left_recipient'], -3, 1, c['one_left_payer'])
        self._new_subscription(c['two_left_self'], -3, 2)
        self._new_subscription(c['two_left_recipient'], -3, 2, c['two_left_payer'])
        self._new_subscription(c['three_left_self'], -3, 3)
        self._new_subscription(c['three_left_recipient'], -3, 3, c['three_left_payer'])
        self._new_subscription(c['stayed_self'], -3, 6)
        self._new_subscription(c['stayed_recipient'], -3, 6, c['stayed_payer'])
        self._new_subscription(c['unstayed_self'], -3, 6)
        self._new_subscription(c['unstayed_recipient'], -3, 6, c['unstayed_payer'])
        self._new_subscription(c['deceased_self'], -3, 6)
        self._new_subscription(c['deceased_recipient'], -3, 6, c['deceased_payer'])
        self._new_subscription(c['lost_contact_self'], -3, 6)
        self._new_subscription(c['lost_contact_recipient'], -3, 6, c['lost_contact_payer'])
        self._new_subscription(c['payer_dead_recipient'], -3, 6, c['payer_dead'])
        self._new_subscription(c['payer_lost_recipient'], -3, 6, c['payer_lost'])
        self._new_subscription(c['payer_stayed_recipient'], -3, 6, c['payer_stayed'])
        self._new_subscription(c['payer_unstayed_recipient'], -3, 6, c['payer_unstayed'])

        self._new_subscription(c['multiple_copies_zero_left'], -3, 0)
        self._new_subscription(c['multiple_copies_one_left'], -3, 1)
        self._new_subscription(c['multiple_copies_two_left'], -3, 2)
        self._new_subscription(c['multiple_copies_lapsed'], -5, -2)

        self._new_mail_stay(c['stayed_self'], -2)
        self._new_mail_stay(c['stayed_recipient'], -2)
        self._new_mail_stay(c['payer_stayed'], -2)

        self._new_mail_stay(c['unstayed_self'], -2, -1)
        self._new_mail_stay(c['unstayed_recipient'], -2, -1)
        self._new_mail_stay(c['payer_unstayed'], -2, -1)
        self.contacts = c

#
# Mail rule basics
#

class TestMailRulesBasics(SubscriberFixtureTestCase):
    def test_fixture_correctness(self):
        c = self.contacts
        remaining = {
            0: ['lapsed', 'zero_left'],
            1: ['one_left'],
            2: ['two_left'],
            3: ['three_left'],
            6: ['stayed', 'unstayed', 'deceased', 'lost_contact'],
        }
        for count, contacts in list(remaining.items()):
            for key in contacts:
                for rel in ("self", "recipient"):
                    contact = self.contacts["%s_%s" % (key, rel)]
                    self.assertEqual(contact.issues_remaining, count)
        multiples = {
            0: ['lapsed', 'zero_left'],
            1: ['one_left'],
            2: ['two_left']
        }
        for count, keys in list(multiples.items()):
            for key in keys:
                contact = self.contacts["multiple_copies_%s" % key]
                self.assertEqual(contact.copies, 2)
                self.assertEqual(contact.issues_remaining, count * 2)
                self.assertEqual(Mailing.objects.filter(contact=contact).count(), 8)

    def test_stayed(self):
        c = self.contacts
        self.assertEqual(set(m.contact for m in MailingStay.objects.stayed()), set([
            c['payer_stayed'], c['stayed_self'], c['stayed_recipient']
        ]))

    def test_reachable(self):
        c = self.contacts
        unreachable = set([c['payer_stayed'], c['stayed_self'], c['stayed_recipient'],
            c['lost_contact_self'], c['lost_contact_recipient'], c['payer_lost'],
            c['deceased_self'], c['deceased_recipient'], c['payer_dead']])
        self.assertEqual(set(Contact.objects.unreachable()), unreachable)
        self.assertEqual(
            set(Contact.objects.reachable()),
            set(self.contacts.values()) - unreachable
        )

#
# Issue mailings
#

class TestIssuesNeededRules(SubscriberFixtureTestCase):
    def test_issues_needed(self):
        issue = Issue(volume=1, number=1, date=now())
        c = self.contacts
        self.assertEqual(
            set(mail_rules.IssuesNeeded(issue=issue).qs),
            set([
                c['perpetual_self'],
                c['one_left_self'],
                c['two_left_self'],
                c['three_left_self'],
                c['perpetual_recipient'],
                c['one_left_recipient'],
                c['two_left_recipient'],
                c['three_left_recipient'],
                c['payer_dead_recipient'],
                c['payer_lost_recipient'],
                c['payer_stayed_recipient'],
                c['payer_unstayed_recipient'],
                c['unstayed_self'],
                c['unstayed_recipient'],
                c['multiple_copies_one_left'],
                c['multiple_copies_two_left'],
            ])
        )

    def test_issues_unreachable(self):
        issue = Issue(volume=1, number=1, date=now())
        c = self.contacts
        self.assertEqual(
            set(mail_rules.IssuesUnreachable(issue=issue).qs),
            set([
                c['deceased_self'],
                c['deceased_recipient'],
                c['stayed_self'],
                c['stayed_recipient'],
                c['lost_contact_self'],
                c['lost_contact_recipient'],
            ])
        )
    def test_issues_enqueued(self):
        sender = User.objects.create(username="admin")
        issue = Issue.objects.create(volume=1, number=1, date=now())
        c = self.contacts
        remaining_map = {}
        for k,contact in c.items():
            remaining_map[k] = contact.issues_remaining

        enqueued = mail_rules.IssuesEnqueued(issue=issue)
        needed = mail_rules.IssuesNeeded(issue=issue)
        sent = mail_rules.IssuesSent(issue=issue)
        unreachable = mail_rules.IssuesUnreachable(issue=issue)

        to_send = set(needed.qs.all())
        # unreachable don't appear in needed.
        self.assertTrue(needed.count() > 0)
        self.assertTrue(unreachable.count() > 0)
        self.assertEqual(set(needed.qs.all()) &
                          set(unreachable.qs.all()), set())
        # enqueued and sent are empty.
        self.assertEqual(set(enqueued.qs.all()), set())
        self.assertEqual(set(sent.qs.all()), set())

        # Transition needed to enqueued
        needed.transition(sender)
        self.assertEqual(set(needed.qs.all()), set())
        self.assertEqual(set(enqueued.qs.all()), to_send)
        self.assertEqual(set(sent.qs.all()), set())

        # Transition enqueued to sent
        enqueued.transition(sender)
        self.assertEqual(set(needed.qs.all()), set())
        self.assertEqual(set(enqueued.qs.all()), set())
        self.assertEqual(set(sent.qs.all()), to_send)

        # Ensure multiple copies are handled.
        for contact in sent.qs.all():
            self.assertEqual(
                Mailing.objects.filter(issue=issue, contact=contact).count(),
                contact.copies
            )


        for k, remaining in remaining_map.items():
            # refresh for latest issues remaining.
            contact = Contact.objects.get(pk=c[k].pk)
            if remaining == -1:
                self.assertEqual(contact.issues_remaining, -1)
                self.assertTrue(contact in to_send)
            elif remaining == 0:
                self.assertEqual(contact.issues_remaining, 0)
                self.assertFalse(contact in to_send)
            elif contact in to_send:
                self.assertEqual(contact.issues_remaining, remaining - contact.copies)
            else:
                self.assertEqual(contact.issues_remaining, remaining)
#
# Subscription expiration notices
#

class TestNoticeNeededRules(SubscriberFixtureTestCase):
    def setUp(self):
        super(TestNoticeNeededRules, self).setUp()
        c = self.contacts

    def test_has_num_left_after_current(self):
        def _has_num_left(when):
            c = self.contacts
            self.assertEqual(
                set(Contact.objects._has_num_left_after_current(0, when=when)),
                set([c['zero_left_recipient'],
                     c['zero_left_self'],
                     c['multiple_copies_zero_left']])
            )
            self.assertEqual(
                set(Contact.objects._has_num_left_after_current(1, when=when)),
                set([c['one_left_recipient'],
                     c['one_left_self'],
                     c['multiple_copies_one_left']])

            )
            self.assertEqual(
                set(Contact.objects._has_num_left_after_current(2, when=when)),
                set([c['two_left_recipient'],
                     c['two_left_self'],
                     c['multiple_copies_two_left']])
            )
        
        # Run once within the 10-day window for 'issue_imminent=False'
        when = self.issues[-1].date + datetime.timedelta(days=9)
        _has_num_left(when)

        # Remove the prior issue, and then run again with the advanced date
        # that "anticipates" the forthcoming issue.
        self.issues[-1].delete()
        #XXX This needs to denormalize better - we shouldn't need to do this...
        for k,c in list(self.contacts.items()):
            c.set_issues_remaining()
        _has_num_left(when)

    def test_notice_needed(self):
        c = self.contacts
        when = self.issues[-1].date + datetime.timedelta(days=9)
        def _needed_matches(remaining, rel, keys):
            self.assertEqual(
                set(Contact.objects.notice_needed(
                        remaining=remaining,
                        rel=rel,
                        when=when)),
                set([c[key] for key in keys]))

        _needed_matches(0, "recipient", [
            'zero_left_recipient', 'zero_left_self', 'multiple_copies_zero_left'
        ])
        _needed_matches(1, "recipient", [
            'one_left_recipient', 'one_left_self', 'multiple_copies_one_left'
        ])
        _needed_matches(2, "recipient", [
            'two_left_recipient', 'two_left_self', 'multiple_copies_two_left'
        ])
        _needed_matches(0, "payer", ['zero_left_payer'])
        _needed_matches(1, "payer", ['one_left_payer'])
        _needed_matches(2, "payer", ['two_left_payer'])

    def test_n_issues_left_mailings(self):
        sender = User.objects.create(username="admin")
        c = self.contacts
        expected = {
            0: {
                'recipient': set([
                    c['zero_left_recipient'], c['zero_left_self'],
                    c['multiple_copies_zero_left'],
                ]),
                'payer': set([c['zero_left_payer']]),
            },
            1: {
                'recipient': set([
                    c['one_left_recipient'], c['one_left_self'],
                    c['multiple_copies_one_left'],
                ]),
                'payer': set([c['one_left_payer']]),
            },
            2: {
                'recipient': set([
                    c['two_left_recipient'], c['two_left_self'],
                    c['multiple_copies_two_left'],
                ]),
                'payer': set([c['two_left_payer']]),
            },
        }

        when = self.issues[-1].date + datetime.timedelta(days=9)

        for n in (0, 1, 2):
            for rel in ("recipient", "payer"):
                needed = mail_rules.NoticeNeeded(n, rel,
                        issue=self.issues[-1], when=when)
                unreachable = mail_rules.NoticeUnreachable(n, rel,
                        issue=self.issues[-1], when=when)
                enqueued = mail_rules.NoticeEnqueued(n, rel,
                        issue=self.issues[-1], when=when)
                sent = mail_rules.NoticeSent(n, rel,
                        issue=self.issues[-1], when=when)

                self.assertEqual(set(needed.qs.all()), expected[n][rel])
                self.assertEqual(set(unreachable.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), set())
                self.assertEqual(set(sent.qs.all()), set())

                needed.transition(sender)
                self.assertEqual(set(needed.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), expected[n][rel])
                self.assertEqual(set(sent.qs.all()), set())

                enqueued.transition(sender)
                self.assertEqual(set(needed.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), set())
                self.assertEqual(set(sent.qs.all()), expected[n][rel])

        # Do the next month.

        new_issue = Issue.objects.create(
            volume=self.issues[-1].volume,
            number=self.issues[-1].number + 1,
            date=self.issues[-1].date + datetime.timedelta(days=30)
        )
        issues_needed = mail_rules.IssuesNeeded(issue=new_issue)
        issues_needed.transition(sender)
        issues_enqueued = mail_rules.IssuesEnqueued(issue=new_issue)
        issues_enqueued.transition(sender)

        for n in (1, 2):
            for rel in ("recipient", "payer"):
                needed = mail_rules.NoticeNeeded(n - 1, rel, issue=new_issue)
                unreachable = mail_rules.NoticeUnreachable(n - 1, rel, issue=new_issue)
                enqueued = mail_rules.NoticeEnqueued(n - 1, rel, issue=new_issue)
                sent = mail_rules.NoticeSent(n - 1, rel, issue=new_issue)

                self.assertEqual(set(needed.qs.all()), expected[n][rel])
                self.assertEqual(set(unreachable.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), set())
                # Sent will include other things near this, because we're not
                # fudging the 'sent' dates, and it's date-based.
                #self.assertEquals(set(sent.qs.all()), set())

                needed.transition(sender)
                self.assertEqual(set(needed.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), expected[n][rel])
                # Sent will include other things near this, because we're not
                # fudging the 'sent' dates, and it's date-based.
                #self.assertEquals(set(sent.qs.all()), set())

                enqueued.transition(sender)
                self.assertEqual(set(needed.qs.all()), set())
                self.assertEqual(set(enqueued.qs.all()), set())
                # Sent will include other things near this, because we're not
                # fudging the 'sent' dates, and it's date-based.
                #self.assertEquals(set(sent.qs.all()), expected[n][rel])



#
# Inquiry mailings
#

class TestInquiryResponseRules(SubscriberFactoryMethods, TestCase):
    def test_inquiry_response(self):
        sender = User.objects.create(username='admin')
        contact = self._new_contact(name="Question Asker")
        unreach = self._new_contact(name="Unreachable Asker", deceased=True)
        irq = Inquiry.objects.create(
                contact=contact,
                request_type=InquiryType.objects.all()[0],
                response_type=InquiryResponseType.objects.filter(
                    generate_mailing=True
                )[0],
        )
        unreach_irq = Inquiry.objects.create(
                contact=unreach,
                request_type=InquiryType.objects.all()[0],
                response_type=InquiryResponseType.objects.filter(
                    generate_mailing=True
                )[0],
        )

        irn = mail_rules.InquiryResponseNeeded()
        ire = mail_rules.InquiryResponseEnqueued()
        irs = mail_rules.InquiryResponseSent()
        iru = mail_rules.InquiryResponseUnreachable()

        self.assertEqual(set(irn.qs.all()), set([irq]))
        self.assertEqual(set(ire.qs.all()), set())
        self.assertEqual(set(irs.qs.all()), set())
        self.assertEqual(set(iru.qs.all()), set([unreach_irq]))

        irn.transition(sender)
        self.assertEqual(set(irn.qs.all()), set())
        self.assertEqual(set(ire.qs.all()), set([irq]))
        self.assertEqual(set(irs.qs.all()), set())
        self.assertEqual(set(iru.qs.all()), set([unreach_irq]))

        ire.transition(sender)
        self.assertEqual(set(irn.qs.all()), set())
        self.assertEqual(set(ire.qs.all()), set())
        self.assertEqual(set(irs.qs.all()), set([irq]))
        self.assertEqual(set(iru.qs.all()), set([unreach_irq]))
#
# Generic mailings
#

class TestGenericMailRules(SubscriberFixtureTestCase):
    def test_generic_mail(self):
        sender = User.objects.create(username='admin')
        contact = self._new_contact(name="Question Asker")
        unreach = self._new_contact(name="Unreachable Asker", deceased=True)

        Mailing.objects.create(
                type=MailingType.objects.get(type='Welcome'),
                contact=contact)
        Mailing.objects.create(
                type=MailingType.objects.get(type='Welcome'),
                contact=unreach)

        issue = Issue.objects.create(volume=0, number=0, date=now())
        other = mail_rules.OtherEnqueuedMail(issue)
        self.assertEqual(set(other.qs.all()), set([contact]))

class TestReachable(SubscriberFactoryMethods, TestCase):
    def test_reachable(self):
        # reachable
        vanilla = self._new_contact("Reachable Regular")
        unstayed = self._new_contact("Was Stayed")
        self._new_mail_stay(unstayed, -3, -1)
        oldaddresses = self._new_contact("Old Addresses")
        for i in range(3):
            ContactAddress.objects.create(
                contact=oldaddresses,
                address=Address.objects.create(address1="Blah", city="Foo"),
                start_date=now() - datetime.timedelta(days=365 * i),
                end_date=now() - datetime.timedelta(days=180 * i)
            )

        # Unreachable
        dead = self._new_contact("Unreachable Deceased", deceased=True)
        lost_contact = self._new_contact("Unreachable LostContact", lost_contact=True)
        stayed = self._new_contact("Unreachable Stayed")
        self._new_mail_stay(stayed, -2)

        noaddress = self._new_contact("No Address")
        for addy in noaddress.contactaddress_set.all():
            addy.delete()
        noaddress = Contact.objects.get(pk=noaddress.pk)
        self.assertEqual(noaddress.last_address_date, None)

        expiredaddress = self._new_contact("Expired Address")
        addy = expiredaddress.contactaddress_set.get()
        addy.start_date = now() - datetime.timedelta(days=365)
        addy.end_date = now() - datetime.timedelta(days=180)
        addy.save()

        self.assertEqual(set(Contact.objects.reachable()),
            set([vanilla, unstayed, oldaddresses]))

        self.assertEqual(set(Contact.objects.unreachable()),
            set([dead, lost_contact, stayed, noaddress, expiredaddress]))


@override_settings(SUBSCRIPTION_CUTOFF='2010-01-01T00:00:00Z')
class TestIssueCountAdjustment(SubscriberFactoryMethods, TestCase):
    """
    Make sure issue counts and recalculation for:
     - old style perpetual
     - new style perpetual
     - subs before, straddling, after cutoff date
     - with no issues after
     - with issues after
    """
    def setUp(self):
        self.contacts = dict(
                old_perpetual=self._new_contact("OldStyle Perpetual"),
                all_before=self._new_contact("All Before"),
                straddling=self._new_contact("Straddling Cutoff"),
                straddling_renewed=self._new_contact("Straddling Renewed"),
                new_style=self._new_contact("New Style"),
                new_perpetual=self._new_contact("NewStyle Perpetual"),
        )
        c = self.contacts
        d = lambda datestr: datestr + "T00:00:00Z"

        # Old style perpetual
        Subscription.objects.create(contact=c['old_perpetual'],
            payer=c['old_perpetual'],
            start_date=d("2008-10-01"), end_date=None, number_of_issues=-1)

        # All before
        Subscription.objects.create(contact=c['all_before'], payer=c['all_before'],
            start_date=d("2007-10-01"), end_date=d("2008-10-01"), number_of_issues=12)
        Subscription.objects.create(contact=c['all_before'], payer=c['all_before'],
            start_date=d("2008-10-01"), end_date=d("2009-10-01"), number_of_issues=12)

        # Straddling
        Subscription.objects.create(contact=c['straddling'], payer=c['straddling'],
            start_date=d("2008-10-01"), end_date=d("2009-10-01"), number_of_issues=12)
        Subscription.objects.create(contact=c['straddling'], payer=c['straddling'],
            start_date=d("2009-10-01"), end_date=d("2010-10-01"), number_of_issues=12)

        # Straddling renewed
        Subscription.objects.create(contact=c['straddling_renewed'],
            payer=c['straddling_renewed'],
            start_date=d("2009-02-01"), end_date=d("2010-02-01"), number_of_issues=12)
        Subscription.objects.create(contact=c['straddling_renewed'],
            payer=c['straddling_renewed'],
            start_date=d("2010-02-01"), end_date=None, number_of_issues=12)

        # New style
        Subscription.objects.create(contact=c['new_style'], payer=c['new_style'],
            start_date=d("2010-01-01"), end_date=None, number_of_issues=12)

        # New perpetual
        Subscription.objects.create(contact=c['new_perpetual'], payer=c['new_perpetual'],
            start_date=d("2010-01-01"), end_date=None, number_of_issues=-1)

        # Issues
        issue_2009_09 = Issue.objects.create(date=d("2009-09-15"), volume=0, number=0)
        issue_2010_10 = Issue.objects.create(date=d("2009-10-15"), volume=0, number=0)
        issue_2010_11 = Issue.objects.create(date=d("2009-11-15"), volume=0, number=0)
        issue_2010_12 = Issue.objects.create(date=d("2009-12-15"), volume=0, number=0)
        # Mailings
        mail_map = {
            issue_2009_09: ('all_before', 'old_perpetual'),
            issue_2010_10: ('old_perpetual', 'straddling', 'straddling_renewed'),
            issue_2010_11: ('old_perpetual', 'straddling', 'straddling_renewed'),
            issue_2010_12: ('old_perpetual', 'straddling', 'straddling_renewed'),
        }
        for issue, keys in mail_map.items():
            for key in keys:
                Mailing.objects.create(contact=c[key], sent=issue.date,
                        type=MailingType.objects.get(type='Issue'), issue=issue)

    def _assert_correct_issues_remaining(self, post_adjust=0):
        c = self.contacts
        self.assertEqual(c['old_perpetual'].issues_remaining, -1)
        self.assertEqual(c['all_before'].issues_remaining, 0)
        self.assertEqual(c['straddling'].issues_remaining, 9 - post_adjust)
        self.assertEqual(c['straddling_renewed'].issues_remaining, 13 - post_adjust)
        self.assertEqual(c['new_style'].issues_remaining, 12 - post_adjust)
        self.assertEqual(c['new_perpetual'].issues_remaining, -1)

    def _assert_correct_issue_adjustment(self):
        c = self.contacts
        self.assertEqual(c['old_perpetual'].issue_adjustment, 0)
        self.assertEqual(c['all_before'].issue_adjustment, -24)
        self.assertEqual(c['straddling'].issue_adjustment, -15)
        self.assertEqual(c['straddling_renewed'].issue_adjustment, -11)
        self.assertEqual(c['new_style'].issue_adjustment, 0)
        self.assertEqual(c['new_perpetual'].issue_adjustment, 0)

    def _assert_correct_sub_number_of_issues(self):
        counts = {
            'old_perpetual': (-1,),
            'all_before': (12, 12),
            'straddling_renewed': (12, 12),
            'new_style': (12,),
            'new_perpetual': (-1,),
        }
        for key, counts in counts.items():
            subs = self.contacts[key].subscription_set.order_by('start_date')
            for sub, count in zip(subs, counts):
                self.assertEqual(sub.number_of_issues, count)

    def _refresh_contacts(self):
        for key, contact in self.contacts.items():
            self.contacts[key] = Contact.objects.get(pk=contact.pk)
