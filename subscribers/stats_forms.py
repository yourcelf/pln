import datetime
from django.utils.timezone import now
from collections import defaultdict
from django.conf import settings

from django import forms
from django.db.models import F
from subscribers.models import Issue, Prison, ContactAddress, Mailing, Subscription
from subscribers.forms import USPSSelectWithEmpty

class LocationAndDateForm(forms.Form):
    city = forms.CharField(required=False)
    state = forms.CharField(widget=USPSSelectWithEmpty(), required=False)
    prison = forms.ModelChoiceField(queryset=Prison.objects.all(),
            widget=forms.TextInput(),
            required=False)
    start_date = forms.DateField(required=False, widget=forms.TextInput)
    end_date = forms.DateField(required=False, widget=forms.TextInput)

    def clean_city(self):
        return self.cleaned_data.get("city") or self.data.get("prison-city")

    def clean_state(self):
        return self.cleaned_data.get("state") or self.data.get("prison-state")

    def clean_start_date(self):
        if self.cleaned_data.get('start_date') is not None:
            return self.cleaned_data['start_date']
        return datetime.date(1990, 1, 1)

    def clean_end_date(self):
        if self.cleaned_data.get('end_date') is not None:
            return self.cleaned_data['end_date']
        return now().date

    def _get_cads(self):
        cd = self.cleaned_data
        cads = ContactAddress.objects.all()
        if cd['prison']:
            cads = cads.filter(prison=cd['prison'])
        else:
            cads = cads.filter(prison__isnull=False)
        if cd['state']:
            cads = cads.filter(prison__address__state=cd['state'])
        if cd['city']:
            cads = cads.filter(prison__address__city=cd['city'])
        cads.exclude(start_date__gt=cd['end_date'])
        cads.exclude(end_date__lt=cd['start_date'])
        return cads

    def get_contact_id_set(self):
        # It's WAY faster to do this with an id__in style request with two
        # queries than to do it as a single query for contacts. The
        # downside: this "constrain" function executes a DB query which it
        # otherwise wouldn't, and while it's cheaper, it isn't the
        # cheapest.
        cd = self.cleaned_data
        has_address = cd['prison'] or cd['state'] or cd['city']

        return set(self._get_cads().values_list('contact_id', flat=True))

class ReceivedIssue(LocationAndDateForm):
    issue = forms.ModelChoiceField(required=False, empty_label="any issue",
            queryset=Issue.objects.all())

    def get_contact_id_set(self):
        contact_id_set = super(ReceivedIssue, self).get_contact_id_set()
        cd = self.cleaned_data
        mailings = Mailing.objects.filter(type__type="Issue", sent__isnull=False)
        if cd['issue']:
            mailings = mailings.filter(issue=cd['issue'])
        mailings = mailings.filter(sent__gte=cd['start_date'], sent__lte=cd['end_date'])
        mailing_contact_ids = mailings.values_list('contact_id', flat=True)
        return contact_id_set & set(mailing_contact_ids)

class PaidSubscription(LocationAndDateForm):
    def get_contact_id_set(self):
        cd = self.cleaned_data
        subs = Subscription.objects.filter(
                start_date__gte=cd['start_date'],
                start_date__lte=cd['end_date'],
                payer_id=F('contact__id')
        ).exclude(type="trial").values_list('contact_id', 'start_date')

        cads = self._get_cads().values_list('contact_id', 'start_date', 'end_date')
        now_date = now().date

        cad_map = defaultdict(list)
        for contact_id, start_date, end_date in cads:
            cad_map[contact_id].append((start_date, end_date or now_date))

        matched = set()
        for contact_id, sub_date in subs:
            for start_date, end_date in cad_map[contact_id]:
                if start_date <= sub_date and sub_date <= end_date:
                    matched.add(contact_id)
                    break
        return matched

class DeliveryAttempt(LocationAndDateForm):
    def get_contact_id_set(self):
        contact_id_set = super(DeliveryAttempt, self).get_contact_id_set()
        cd = self.cleaned_data
        mailings = Mailing.objects.filter(sent__isnull=False)
        mailings.filter(sent__gte=cd['start_date'], sent__lte=cd['end_date'])
        mailing_contact_ids = mailings.values_list('contact_id', flat=True)
        return contact_id_set & set(mailing_contact_ids)

class LivedIn(LocationAndDateForm):
    def get_contact_id_set(self):
        return super(LivedIn, self).get_contact_id_set()

class SubscriptionExpired(LocationAndDateForm):
    def get_contact_id_set(self):
        #TODO
        raise NotImplementedError

        contact_id_set = super(SubscriptionExpired, self).get_contact_id_set()
        cd = self.cleaned_data

        # We have two strategies: looking at "subscription.end_date" for
        # subscriptions ending before settings.SUBSCRIPTION_CUTOFF,
        # and counting issues for subscriptions after that.  Historical
        # subscriptions had data errors in the counts of issues.

        if cd['start_date'] <= settings.SUBSCRIPTION_CUTOFF:
            # While the old "subscription.end_date" strategy is much simpler,
            # it also ignores complexity introduced by mailing stays and such.
            contact_ids = Subscription.objects.filter(
                    end_date__gte=cd['start_date'],
                    end_date__lte=cd['end_date']).values_list('contact_id')
        if cd['end_date'] >= settings.SUBSCRIPTION_CUTOFF:
            # This strategy is much more complex.  The "end" of a subscription
            # is nothing more than a month in which there are zero issues left
            # which follows a month in which there were more than zero issues
            # left.  To do this, count the mailings sent since the cutoff,
            # which will average a few thousand per month.

            subs = Subscription.objects.filter(
                    start_date__gte=settings.SUBSCRIPTION_CUTOFF)
            mailings = Mailing.objects.filter(
                    sent__gte=max(settings.SUBSCRIPTION_CUTOFF, cd['start_date']),
                    sent__lte=cd['end_date'],
                    type__type="Issue")



