import os
import sys
import subprocess

from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings

GIT_BASE = os.path.join(settings.BASE_DIR, "..")
GIT_PATH = getattr(settings, "GIT_PATH", "git")

class Command(BaseCommand):
    help = "Pull git, and execute all management commands needed to update everything at once."

    def handle(self, *args, **kwargs):
        proc = subprocess.Popen([GIT_PATH, "pull", "origin", "master"],
                cwd=GIT_BASE,
                stdout=self.stdout,
                stderr=self.stderr,
                stdin=subprocess.PIPE)
        proc.communicate()

        call_command('migrate', interactive=False,
                stdout=self.stdout, stderr=self.stderr)
        call_command('collectstatic', interactive=False,
                stdout=self.stdout, stderr=self.stderr)
