import os
import subprocess
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        filename = os.path.join(
            os.path.dirname(__file__), "..", '..', '..', 'backups',
            datetime.datetime.now().strftime("%Y-%m-%dT%H.%M.%S")
        )
        if len(args) > 0:
            filename += "_" + args[0]
        filename += ".dump"

        try:
            os.putenv('PGPASSWORD', settings.DATABASES['default']['PASSWORD'])
            subprocess.check_call(" ".join([
                '"%s"' % settings.PG_DUMP_PATH,
                "-h", settings.DATABASES['default']['HOST'],
                "-p", settings.DATABASES['default']['PORT'], 
                "-U", settings.DATABASES['default']['USER'],
                "-F", "c",
                "-b",
                "-f", filename,
                settings.DATABASES['default']["NAME"],
            ]), shell=True)
            print(("Generated backup:", os.path.basename(filename)))
        except Exception as e:
            raise CommandError(e)
