import json
import urllib.request, urllib.parse, urllib.error
import datetime
from collections import defaultdict
import tempfile
import traceback
import re

from django.db.models import Q, Count
from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.timezone import now, make_aware, utc
from django.db import transaction
from django.http import HttpResponse, HttpResponseBadRequest, Http404
from django.utils.html import linebreaks
from django.utils.dateparse import parse_datetime
from django.core.management import call_command

from openpyxl import load_workbook

from subscribers.models import *
from subscribers import forms, stats_forms
from subscribers.utils import institution_xlsx_response, contact_xlsx_response, \
        contact_pdf_response
from subscribers import mail_rules, inquiry_response_actions

CONTACT_SEARCH_DEFAULTS = {
    'deceased': False,
    'lost_contact': False,
    "sort": "last_name",
    'exclude_missing_names': True,
    'exclude_addressless': True,
}

def _update_default_date():
    CONTACT_SEARCH_DEFAULTS['search_addresses_current_on_date'] = now().strftime("%Y-%m-%d")

def _paginate(request, object_list, per_page=10):
    paginator = Paginator(object_list, per_page)
    try:
        paged = paginator.page(request.GET.get('page'))
    except PageNotAnInteger:
        paged = paginator.page(1)
    except EmptyPage:
        paged = paginator.page(paginator.num_pages)

    queryargs = {}
    for key in request.GET:
        queryargs[key] = request.GET.get(key)
    queryargs.pop('page', None)
    if queryargs:
        querystr = '?%s&' % urllib.parse.urlencode(queryargs)
    else:
        querystr = '?'
    paged.querystr = querystr
    show_numbers = sorted(list(set(
        list(range(1, min(6, paginator.num_pages + 1))) +
        list(range(max(1, paged.number - 3), min(paged.number + 3, paginator.num_pages + 1))) +
        list(range(max(1, paginator.num_pages - 5), paginator.num_pages + 1))
    )))
    pageiter = []
    prev = 0
    for number in show_numbers:
        if number != prev + 1:
            pageiter.append(("", "..."))
        pageiter.append((True, number))
        prev = number

    paged.pageiter = pageiter

    return paged

def _date_aggregate(queryset, date_field, precision='week'):
    return queryset.extra(
            select={precision: """DATE_TRUNC('%s', %s)""" % (precision, date_field)}
        ).values(
            precision
        ).annotate(count=Count('pk')).order_by(precision)

#
# Contacts
#

@login_required
def contacts(request):
    _update_default_date()
    form_params = {}
    form_params.update(CONTACT_SEARCH_DEFAULTS)
    for key in request.GET:
        if key == "tag_search":
            form_params[key] = request.GET.getlist(key)
        else:
            form_params[key] = request.GET.get(key)
    form = forms.ContactSearch(form_params)
    contacts = Contact.objects.all().order_by('-created').select_related(
            'organization_type', 'source'
    )

    if form.is_valid():
        contacts = form.constrain(contacts)
        if request.GET.get('xlsx', None):
            return contact_xlsx_response(contacts, fmt="default")
        if request.GET.get('pdf', None):
            return contact_pdf_response(contacts,
                    subdetails=request.GET.get('subdetails')=='1')
        if request.GET.get('mailing', None):
            form = forms.MailingTypeForm(request.POST or None)
            if form.is_valid():
                with transaction.atomic():
                    mtype = form.cleaned_data['mailing_type']
                    log = MailingLog.objects.create(
                        user=request.user,
                        type=mtype,
                        note=form.cleaned_data['note'] or "From search results",
                    )
                    for contact in contacts:
                        log.mailings.create(type=mtype,
                                contact=contact,
                                sent=now())
                return redirect(
                    reverse('subscribers.mailinglogs.show', args=[log.pk])
                )
            return render(request, "mailings/create_from_search.html", {
                "contacts": _paginate(request, contacts),
                "form": form,
            })
    return render(request, "contacts/list.html", {
        'form': form,
        'contacts': _paginate(request, contacts),
    })

@login_required
@transaction.atomic
def show_contact(request, contact_id):
    contact = get_object_or_404(Contact, pk=contact_id)
    #
    # Bind forms to POST only if the appropriate submit button was pressed.
    #
    if request.POST.get("edit_contact", None):
        contact_form = forms.ContactAddForm(request.POST, instance=contact)
    else:
        contact_form = forms.ContactAddForm(None, instance=contact)

    # Add address (non-prison address) form
    if request.POST.get("add_address", None):
        address_form = forms.AddressForm(request.POST)
    else:
        address_form = forms.AddressForm(None)

    # Add contactaddress form (for prison addresses)
    if request.POST.get("add_contactaddress", None):
        contactaddress_form = forms.ContactAddressForm(request.POST)
    else:
        contactaddress_form = forms.ContactAddressForm(None)

    # Add subscription form
    if request.POST.get("add_subscription", None):
        subscription_form = forms.SubscriptionForm(request.POST)
    else:
        subscription_form = forms.SubscriptionForm(None)

    # Add other purchase form
    other_purchase_instance = OtherPurchase(contact=contact)
    if request.POST.get("add_other_purchase", None):
        other_purchase_form = forms.OtherPurchaseForm(request.POST,
                instance=other_purchase_instance,
                prefix="add_other_purchase")
    else:
        other_purchase_form = forms.OtherPurchaseForm(None,
                instance=other_purchase_instance,
                prefix="add_other_purchase")

    # Add mailing stay form
    mailing_stay_instance = MailingStay(contact=contact)
    if request.POST.get("add_mailing_stay", None):
        mailing_stay_form = forms.MailingStayForm(request.POST, prefix="mailingstay",
                instance=mailing_stay_instance)
    else:
        mailing_stay_form = forms.MailingStayForm(None, prefix="mailingstay",
                instance=mailing_stay_instance)

    # Add inquiry form
    inquiry_instance = Inquiry(contact=contact)
    if request.POST.get("add_inquiry", None):
        inquiry_form = forms.InquiryForm(request.POST,
                instance=inquiry_instance)
    else:
        inquiry_form = forms.InquiryForm(None,
                instance=inquiry_instance)

    # Add mailing form
    mailing_instance = Mailing(contact=contact)
    if request.POST.get("add_mailing", None):
        mailing_form = forms.MailingForm(request.POST,
                prefix="mailing", instance=mailing_instance)
    else:
        mailing_form = forms.MailingForm(None,
                prefix="mailing", instance=mailing_instance)

    #
    # Handle form posting.
    #
    if request.method == 'POST':
        right_now = now()

        # Address
        cad_is_valid = "add_contactaddress" in request.POST and contactaddress_form.is_valid()
        add_is_valid = "add_address" in request.POST and address_form.is_valid()
        if cad_is_valid or add_is_valid:
            cad = contact.current_address()
            if cad:
                cad.end_date = right_now
                cad.save()
            if cad_is_valid:
                contactaddress_form.create_for(contact)
            elif add_is_valid:
                ContactAddress.objects.create(
                    address=address_form.save(),
                    contact=contact
                )
            return redirect(request.path)
        # New inquiry
        if inquiry_form.is_valid():
            inquiry = inquiry_form.save()
            inquiry_response_actions.handle(inquiry)
            return redirect(request.path)
        # New mailing stay
        if mailing_stay_form.is_valid():
            mailing_stay_form.save()
            return redirect(request.path)
        # New other purchase
        if other_purchase_form.is_valid():
            other_purchase_form.save()
            return redirect(request.path)
        # New subscription
        if subscription_form.is_valid():
            subscription_form.create_for(contact)
            return redirect(request.path)
        # New mailing
        if mailing_form.is_valid():
            mailing = mailing_form.save()
            return redirect(request.path)
        # Change contact
        if contact_form.is_valid():
            contact = contact_form.save()
            return redirect(request.path)

    return render(request, "contacts/show.html", {
        'contact': contact,
        'contact_form': contact_form,
        'address_form': address_form,
        'mailing_form': mailing_form,
        'contactaddress_form': contactaddress_form,
        'subscription_form': subscription_form,
        'inquiry_form': inquiry_form,
        'other_purchase_form': other_purchase_form,
        'mailing_stay_form': mailing_stay_form,
        'payed_for': contact.subscriptions_payed_for.exclude(contact=contact),
    })

@login_required
def end_subscription_early(request, subscription_id):
    subscription = get_object_or_404(Subscription, pk=subscription_id)
    stop_form = forms.StopReasonForm(request.POST or None)
    if stop_form.is_valid():
        subscription.end_early(stop_form.cleaned_data['reason'])
        return redirect(subscription.contact.get_absolute_url())
    return render(request, "contacts/stop_subscription.html", {
        'subscription': subscription,
        'stop_form': stop_form,
        'now': now(),
    })

@login_required
def restart_subscription(request, subscription_id):
    if request.method == 'POST':
        subscription = get_object_or_404(Subscription, pk=subscription_id)
        subscription.restart()
        return HttpResponse("success")
    return HttpResponseBadRequest()

@login_required
@transaction.atomic
def add_contact(request):
    contact_form = forms.ContactAddForm(request.POST or None,
            use_required_attribute=False)
    contactaddress_form = forms.ContactAddressForm(request.POST or None,
            use_required_attribute=False)
    address_form = forms.AddressForm(request.POST or None,
            use_required_attribute=False)

    # Subscription is optional, so only bind form (which will display field
    # errors) if the fields are non-empty.
    sub_form_binding = None
    if request.method == 'POST':
        for field in ("source", "number_of_months", "payer"):
            if request.POST.get("subscription-{0}".format(field)) != "":
                sub_form_binding = request.POST
                break
    subscription_form = forms.SubscriptionForm(sub_form_binding,
            prefix="subscription", use_required_attribute=False)

    # Other Purchase is optional, so only bind form (which will display field
    # errors) if the fields are non-empty.
    other_purchase_binding = None
    if request.method == 'POST':
        for field in ("type", "count"):
            if request.POST.get("otherpurchase-{0}".format(field)) != "":
                other_purchase_binding = request.POST
                break
    other_purchase_form = forms.OtherPurchaseForm(other_purchase_binding,
            prefix="otherpurchase", use_required_attribute=False)

    if contact_form.is_valid() and \
            ((contact_form.cleaned_data['type'] == "prisoner" and
              contactaddress_form.is_valid()) or address_form.is_valid()) and \
            (sub_form_binding is None or subscription_form.is_valid()) and \
            (other_purchase_binding is None or other_purchase_form.is_valid()):
        contact = contact_form.save()
        if contact.type == "prisoner":
            contactaddress_form.create_for(contact)
        else:
            ContactAddress.objects.create(
                    contact=contact,
                    address=address_form.save()
            )
        if sub_form_binding is not None:
            subscription_form.create_for(contact)

        if other_purchase_binding is not None:
            instance = OtherPurchase(contact=contact)
            form_with_instance = forms.OtherPurchaseForm(other_purchase_binding,
                    prefix="otherpurchase", instance=instance,
                    use_required_attribute=False)
            if form_with_instance.is_valid():
                form_with_instance.save()

        return redirect("subscribers.contacts.show", contact.pk)
    return render(request, "contacts/add.html", {
        'contact_form': contact_form,
        'address_form': address_form,
        'contactaddress_form': contactaddress_form,
        'subscription_form': subscription_form,
        'other_purchase_form': other_purchase_form,
    })

@permission_required("subscribers.delete_inquiry")
def delete_inquiry(request, inquiry_id):
    if request.method == 'POST':
        ir = get_object_or_404(Inquiry, pk=inquiry_id)
        url = ir.contact.get_absolute_url()
        ir.delete()
        return redirect(url)
    return HttpResponseBadRequest()

@permission_required("subscribers.delete_other_purchase")
def delete_other_purchase(request, other_purchase_id):
    if request.method == 'POST':
        op = get_object_or_404(OtherPurchase, pk=other_purchase_id)
        url = op.contact.get_absolute_url()
        op.delete()
        return redirect(url)
    return HttpResponseBadRequest()

@login_required
def end_mailing_stay(request, mailingstay_id):
    if request.method == 'POST':
        ms = get_object_or_404(MailingStay, pk=mailingstay_id)
        ms.end_date = now()
        ms.save()
        return HttpResponse("success")
    return HttpResponseBadRequest()
    
@login_required
def donor_batches(request):
    batches = DonorBatch.objects.all()
    contacts = Contact.objects.filter(opt_out=False, donor=True)
    cutoff = (request.POST or request.GET).get("no_contact_since")
    if cutoff:
        contacts = contacts.filter(
                Q(donorbatch__isnull=True) | Q(donorbatch__date__lte=cutoff),
                donor=True,
        )
    if request.method == 'POST':
        if len(contacts) > 0:
            batch = DonorBatch.objects.create()
            batch.contacts.set(contacts)
            batch.save()
        return redirect("subscribers.contacts.donor_batches")
    return render(request, "contacts/donor_batches.html", {
        "batches": batches,
        "contacts": contacts
    })

@login_required
def donor_batch_count(request):
    contacts = Contact.objects.filter(opt_out=False, donor=True)
    cutoff = (request.POST or request.GET).get("no_contact_since")
    if cutoff:
        contacts = contacts.filter(
                Q(donorbatch__isnull=True) | Q(donorbatch__date__lte=cutoff),
                donor=True,
        )
    response = HttpResponse(json.dumps({
        'count': contacts.count()
    }))
    response['Content-Type'] = 'application/json'
    return response

@login_required
def delete_donor_batch(request, donor_batch_id):
    if not request.user.has_perm("subscribers.delete_donorbatch"):
        raise PermissionDenied
    batch = get_object_or_404(DonorBatch, pk=donor_batch_id)
    batch.delete()
    return redirect("subscribers.contacts.donor_batches")

#
# Ajaxy
#

@login_required
def fuzzy_contact_search(request):
    first = request.GET.get('first')
    middle = request.GET.get('middle')
    last = request.GET.get('last')
    prisoner_number = request.GET.get('prisoner_number')
    try:
        index = int(request.GET.get('index'))
    except Exception:
        index = 0
    if not any((first, middle, last, prisoner_number)):
        contacts = Contact.objects.none()
    else:
        if prisoner_number:
            num_contacts = Contact.objects.filter(
                contactaddress__prisoner_number__icontains=prisoner_number)
        else:
            num_contacts = None
        if any((first, middle, last)):
            name_contacts = Contact.objects.similar_to(first, middle, last, 0.4)
        else:
            name_contacts = None

        # Combine the query sets.
        if num_contacts is not None and name_contacts is not None:
            keepers = set([c.id for c in num_contacts]) & set([c.id for c in name_contacts])
            all_contacts = dict((c.id, c) for c in num_contacts if c.id in keepers)
            all_contacts.update(dict((c.id, c) for c in name_contacts if c.id in keepers))
            contacts = list(all_contacts.values())
        else:
            contacts = num_contacts or name_contacts

    rendered = render(request, "contacts/_fuzzy.html", {
        'contacts': contacts[0:6] if contacts else [],
    }).content
    response = HttpResponse(json.dumps({
        "index": index,
        "html": rendered.decode("utf-8"),
    }))
    response['Content-Type'] = 'application/json'
    return response

@login_required
def json_contact_search(request):
    q = request.GET.get('q')
    contacts = Contact.objects.all()
    if q:
        for word in q.split():
            contacts = contacts.filter(
                    Q(first_name__icontains=word) |
                    Q(middle_name__icontains=word) |
                    Q(last_name__icontains=word) |
                    Q(organization_name__icontains=word)
            )
    # Can't use django's built-in pagination here, because similar_to returns
    # an immutable queryset.
    count = contacts.count()
    try:
        per_page = int(request.GET.get('per_page', 10))
    except ValueError:
        return HttpResponseBadRequest("Invalid per_page number.")
    try:
        requested_page = int(request.GET.get('page', 1))
        if requested_page < 1 or (requested_page - 1) * per_page > count:
            raise Http404
    except ValueError:
        return HttpResponseBadRequest("Invalid page number.")
    results = contacts[(requested_page - 1) * per_page : requested_page * per_page]
    response = HttpResponse(json.dumps({
            'results': [{'id': c.pk, 'text': str(c)} for c in results],
            'page': requested_page,
            'count': count,
    }, indent=4))
    response['Content-type'] = "application/json"
    return response

@login_required
def json_prison_search(request):
    q = request.GET.get('q')
    state = request.GET.get('state')
    city = request.GET.get('city')
    pk = request.GET.get('id')
    prisons = Prison.objects.all()
    if pk:
        prisons = prisons.filter(pk=pk)
    else:
        if state:
            prisons = prisons.filter(address__state__icontains=state)
        if city:
            prisons = prisons.filter(address__city__icontains=city)
        if q:
            for word in q.split():
                prisons = prisons.filter(
                        Q(name__icontains=word) |
                        Q(admin_name__icontains=word) |
                        Q(address__city__icontains=word) |
                        Q(address__address1__icontains=word) |
                        Q(address__address2__icontains=word) |
                        Q(address__zip__icontains=word))
    count = prisons.count()
    states = prisons.values_list(
            'address__state', flat=True
        ).order_by('address__state').distinct()
    cities = prisons.values_list(
            'address__city', flat=True
        ).order_by('address__city').distinct()
    try:
        per_page = int(request.GET.get('per_page', 10))
    except ValueError:
        return HttpResponseBadRequest("Invalid per_page number.")
    try:
        requested_page = int(request.GET.get('page', 1))
        if requested_page < 1 or (requested_page - 1) * per_page > count:
            raise Http404
    except ValueError:
        return HttpResponseBadRequest("Invalid page number.")
    results = prisons[(requested_page - 1) * per_page : requested_page * per_page]
    response = HttpResponse(json.dumps({
            'states': list(states),
            'cities': list(cities),
            'results': [{
                'id': p.pk,
                'text': p.format_list_display().replace("\n", "<br />"),
            } for p in results],
            'page': requested_page,
            'count': count,
    }, indent=4))
    response['Content-type'] = "application/json"
    return response

@login_required
def mark_censored(request, mailing_id):
    if request.method != 'POST':
        raise Http404
    mailing = get_object_or_404(Mailing, pk=mailing_id)
    mailing.censored = request.POST.get("censored") == 'on'
    mailing.save()
    return redirect("subscribers.contacts.show", mailing.contact.pk)

@login_required
def delete_contactaddress(request, contactaddress_id):
    if not request.user.has_perm("subscribers.delete_contactaddress"):
        raise PermissionDenied
    if request.method == 'POST':
        get_object_or_404(ContactAddress, pk=contactaddress_id).delete()
        return HttpResponse("success")
    return HttpResponseBadRequest("POST required")

@login_required
def delete_subscription(request, subscription_id):
    if not request.user.has_perm("subscribers.delete_subscription"):
        raise PermissionDenied
    if request.method == 'POST':
        get_object_or_404(Subscription, pk=subscription_id).delete()
        return HttpResponse("success")
    return HttpResponseBadRequest("POST required")

@login_required
def list_notes(request, contact_id, note_id=None, contact=None):
    contact = contact or get_object_or_404(Contact, pk=contact_id)
    if request.method == 'POST':
        text = request.POST.get("text")
        if note_id:
            note = get_object_or_404(Note, pk=note_id)
        else:
            # Guard against double-posting
            try:
                now = datetime.datetime.now()
                note = Note.objects.get(
                        created__year=now.year,
                        created__month=now.month,
                        created__day=now.day,
                        contact=contact,
                        author=request.user,
                        text=text)
            except Note.DoesNotExist:
                note = Note()
        note.contact = contact
        note.author = request.user
        note.text = text
        note.save()
        
    notes = Note.objects.filter(contact=contact)
    return render(request, "_notes_list.html", {
        'contact': contact,
        'notes': notes
    })

@login_required
def delete_note(request, note_id=None):
    if not request.user.has_perm("subscribers.delete_note"):
        raise PermissionDenied
    note = get_object_or_404(Note, pk=note_id)
    note.delete()
    return HttpResponse("success")

#
# Mailings
#

@login_required
def mailings_sent(request):
    page = _paginate(request, MailingLog.objects.order_by('-created'))
    return render(request, "mailings/list_mailinglogs.html", {
        'page': page,
    })

@login_required
def show_mailinglog(request, mailinglog_id):
    log = get_object_or_404(MailingLog, id=mailinglog_id)
    contact_id_to_mailings = defaultdict(list)
    for mailing in log.mailings.all():
        contact_id_to_mailings[mailing.contact_id].append(mailing)
    mailings = Mailing.objects.filter(mailinglog=log).select_related('contact', 'inquiry')
    recipients = []
    for mailing in mailings:
        if mailing.inquiry:
            recipients.append(mailing.inquiry)
        else:
            recipients.append(mailing.contact)
    if request.GET.get("xlsx", False):
        if log.type.type == "Inquiry":
            fmt = "inquiry"
        else:
            fmt = "default"
        return contact_xlsx_response(recipients, fmt=fmt)
    elif request.GET.get("pdf", False):
        return contact_pdf_response(recipients,
                subdetails=request.GET.get("subdetails")=="1")

    contacts = Contact.objects.filter(mailing__mailinglog=log)
    _update_default_date()
    search_form = forms.ContactSearch(request.GET or CONTACT_SEARCH_DEFAULTS)
    if search_form.is_valid():
        contacts = search_form.constrain(contacts)
    return render(request, "mailings/show_mailinglog.html", {
        'log': log,
        'form': search_form,
        'contact_id_to_mailings': contact_id_to_mailings,
        'page': _paginate(request, contacts),
    })
    
@login_required
@transaction.atomic
def delete_mailinglog(request, mailinglog_id):
    if not request.user.has_perm("subscribers.delete_mailinglog"):
        raise PermissionDenied
    if not request.method == 'POST':
        return HttpResponseBadRequest("POST required")
    log = get_object_or_404(MailingLog, id=mailinglog_id)
    for mailing in log.mailings.all():
        mailing.sent = None
        mailing.save()
    log.delete()
    return redirect("subscribers.mailinglogs.list")


@login_required
def mark_mailing_sent(request, mailing_id=None):
    if request.method == "POST":
        mailing = get_object_or_404(Mailing, id=mailing_id)
        mailing.sent = now()
        mailing.save()
        return HttpResponse("success")
    return HttpResponseBadRequest("POST required")

@login_required
@permission_required("subscribers.delete_mailing")
def delete_mailing(request, mailing_id=None):
    if request.method == "POST":
        mailing = get_object_or_404(Mailing, id=mailing_id)
        mailing.delete()
        return HttpResponse("success")
    return HttpResponseBadRequest("POST required")

@login_required
@transaction.atomic
def mailings_needed(request, issue_id=None):
    """
    Handle listing and creation of 3 types of things:
     - Issues
     - Last issue notices
     - Info requests
    Each of these things generate a Mailing object, and this Mailing object
    gets attached to a MailingLog object when sent.

    Each item has 3 states:
     - needed: policy dictates it should exist, but there's no Mailing object.
     - enqueued: a Mailing object exists, but it's not sent.
     - sent: the Mailing object is marked sent.

    MailingLog objects act as a convenient interface by which to group Mailing
    objects for printing labels en masse.  However, it's not considered "sent"
    until the Mailing object has been marked "sent", regardless of belonging
    to a MailingLog object or not.
    """
    latest = Issue.objects.latest('date')
    if issue_id:
        issue = get_object_or_404(Issue, pk=issue_id)
        is_latest = latest == issue
    else:
        issue = latest
        is_latest = True

    mailings = [
            {
                'name': str("Issue: %s" % issue),
                'needed': mail_rules.IssuesNeeded(issue=issue),
                'unreachable': mail_rules.IssuesUnreachable(issue=issue),
                'enqueued': mail_rules.IssuesEnqueued(issue=issue),
                'sent': mail_rules.IssuesSent(issue=issue),
            },
    ]
    if is_latest:
        mailings += [
            {
                'name': str("0 issues left, recipient"),
                'needed': mail_rules.NoticeNeeded(0, "recipient", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(0, "recipient", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(0, "recipient", issue=issue),
                'sent': mail_rules.NoticeSent(0, "recipient", issue=issue),
            },
            {
                'name': str("1 issues left, recipient"),
                'needed': mail_rules.NoticeNeeded(1, "recipient", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(1, "recipient", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(1, "recipient", issue=issue),
                'sent': mail_rules.NoticeSent(1, "recipient", issue=issue),
            },
            {
                'name': str("2 issues left, recipient"),
                'needed': mail_rules.NoticeNeeded(2, "recipient", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(2, "recipient", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(2, "recipient", issue=issue),
                'sent': mail_rules.NoticeSent(2, "recipient", issue=issue),
            },
            {
                'name': str("0 issues left, payer"),
                'needed': mail_rules.NoticeNeeded(0, "payer", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(0, "payer", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(0, "payer", issue=issue),
                'sent': mail_rules.NoticeSent(0, "payer", issue=issue),
            },
            {
                'name': str("1 issues left, payer"),
                'needed': mail_rules.NoticeNeeded(1, "payer", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(1, "payer", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(1, "payer", issue=issue),
                'sent': mail_rules.NoticeSent(1, "payer", issue=issue),
            },
            {
                'name': str("2 issues left, payer"),
                'needed': mail_rules.NoticeNeeded(2, "payer", issue=issue),
                'unreachable': mail_rules.NoticeUnreachable(2, "payer", issue=issue),
                'enqueued': mail_rules.NoticeEnqueued(2, "payer", issue=issue),
                'sent': mail_rules.NoticeSent(2, "payer", issue=issue),
            },
            {
                'name': str("Inquiry Responses"),
                'needed': mail_rules.InquiryResponseNeeded(),
                'unreachable': mail_rules.InquiryResponseUnreachable(),
                'enqueued': mail_rules.InquiryResponseEnqueued(),
                'sent': mail_rules.InquiryResponseSent(),
            },
            {
                'name': str("Other enqueued mail"),
                'needed': mail_rules.EmptyRule(),
                'unreachable': mail_rules.EmptyRule(),
                'enqueued': mail_rules.OtherEnqueuedMail(issue),
                'sent': mail_rules.EmptyRule(),
            },
        ]

    # Try to resolve the list of whom to show.
    path = request.GET.get("path")
    if path:
        matched_mailing = None
        for mailing_group in mailings:
            for key in ("needed", "unreachable", "enqueued", "sent"):
                if path == mailing_group[key].code:
                    matched_mailing = mailing_group[key]
                    break
            else:
                continue
            break
        else:
            raise Http404
        if request.method == "POST":
            if request.POST.get("action") == matched_mailing.action:
                matched_mailing.transition(request.user)
                return redirect(request.path)
            else:
                return HttpResponseBadRequest("Unmatched action")
        if path.startswith("inquiry"):
            recipients = None
            inquiries = _paginate(request, matched_mailing.qs.order_by('-date'))
        else:
            recipients = matched_mailing.qs
            inquiries = None
            if request.GET.get("xlsx"):
                return contact_xlsx_response(recipients)
            elif request.GET.get("pdf"):
                return contact_pdf_response(recipients)
    else:
        matched_mailing = None
        recipients = None
        inquiries = None

    if recipients is not None:
        search_form_vars = {}
        _update_default_date()
        search_form_vars.update(CONTACT_SEARCH_DEFAULTS)
        for key in request.GET:
            search_form_vars[key] = request.GET.get(key)
        contact_search_form = forms.ContactSearch(search_form_vars)
        recipients = contact_search_form.constrain(recipients)
        page = _paginate(request, recipients)
    else:
        contact_search_form = None
        page = None

    issues = []
    for ruleset in mailings:
        for k,v in ruleset.items():
            if isinstance(v, str):
                continue
            issues.append((k, v.issue))

    return render(request, "mailings/needed.html", {
        'issue': issue,
        'is_latest': is_latest,
        'path': path,
        'mailings': mailings,
        'matched_mailing': matched_mailing,
        'inquiries': inquiries,
        'contact_search_form': contact_search_form,
        'page': page,
        'logs': MailingLog.objects.all().order_by('-created')[0:10],
    })

def institution_labels(request):
    form = forms.InstitutionLabelForm(request.POST or None)
    if form.is_valid():
        prefix = form.cleaned_data['label_prefix']
        addresses = []
        for prison in Prison.objects.select_related('address'):
            if prefix:
                lines = [prefix, prison.name]
            else:
                lines = [prison.name]
            lines += prison.address.get_address_lines()
            addresses.append(lines)
        if request.POST.get("xlsx"):
            return contact_xlsx_response(addresses)
        else:
            return contact_pdf_response(addresses)
    return render(request, "mailings/institution_labels.html", {
        "form": form,
    })

def institution_spreadsheet(request):
    """
    Produce a spreadsheet
    """
    return institution_xlsx_response(Prison.objects.select_related("address"))

#
# Issues
#

@login_required
def show_issue(request, issue_id=None):
    issue = get_object_or_404(Issue, id=issue_id)
    paths = {
        'needed': Contact.objects.issue_needed(issue),
        'unreachable': Contact.objects.issue_unreachable(issue),
        'enqueued': Contact.objects.issue_enqueued(issue),
        'sent': Contact.objects.issue_sent(issue),
    }
    whats = {
            'needed': "Contacts who still need this issue",
            'unreachable': "Contacts who should receive this, but can't be reached",
            'enqueued': "Contacts with this issue enqueued",
            'sent': "Contacts who've been sent this issue",
    }
    path = request.GET.get('path')
    if path:
        recipients = paths[path]
        form_params = {}
        _update_default_date()
        form_params.update(CONTACT_SEARCH_DEFAULTS)
        for key in request.GET:
            form_params[key] = request.GET.get(key)
        form = forms.ContactSearch(form_params)
        if recipients is not None and form.is_valid():
            recipients = form.constrain(recipients)
        page = _paginate(request, recipients)
        what = whats[path]
        if request.GET.get('xlsx'):
            return contact_xlsx_response(recipients)
        elif request.GET.get('pdf'):
            return contact_pdf_response(recipients)
    else:
        page = None
        form = None
        what = None
    return render(request, "issues/show.html", {
        'paths': paths,
        'path': path,
        'what': what,
        'issue': issue,
        'page': page,
        'form': form,
    })
    

@login_required
def list_issues(request):
    form = forms.IssueForm(request.POST or None)
    if form.is_valid():
        issue = form.save()
        return redirect(request.path)
    mailing_counts = defaultdict(int)
    issues = Issue.objects.all()
    issue_type = MailingType.objects.get(type='Issue')
    for issue_id in  Mailing.objects.filter(
                sent__isnull=False, type_id=issue_type.id
            ).order_by('id').values_list('issue_id', flat=True):
        mailing_counts[issue_id] += 1
    return render(request, "issues/list.html", {
        'issues_with_counts': [(issue, mailing_counts[issue.pk]) for issue in issues],
        'form': form,
    })

@login_required
def delete_issue(request, issue_id):
    if not request.user.has_perm("subscribers.delete_issue"):
        raise PermissionDenied
    issue = get_object_or_404(Issue, pk=issue_id)
    if request.method == 'POST':
        for mailing in issue.mailing_set.all():
            mailing.delete()
        issue.delete()
        return redirect("subscribers.issues")
    return HttpResponseBadRequest("POST required")


@login_required
def stats(request):
    return stats_buckets(request)

@login_required
def stats_buckets(request):
    """
    Display records of:
     - how many current subscriptions per month?
     - how many new subscriptions per month?
     - how many renewed subscriptions per month?
     - how many expiring subscriptions per month?
    New is defined as: first subscription for a contact.
    Renew is: nth subscription for a contact created, where n > 1.
    Expiring is: Received previous month's issue, did not receive current month's issue.

    This function ruthlessly ignores stayed subscriptions, and actual records
    of sent mail, and instead just works entirely from the pair of a
    subscriptions "start_date" and its number of issues.
    """

    subs = Subscription.objects.raw("""
        SELECT s.id,
               s.contact_id,
               date_part('year', s.start_date) AS start_year,
               date_part('month', s.start_date) AS start_month,
               s.number_of_issues,
               c.copies
            FROM subscribers_subscription s
            LEFT OUTER JOIN subscribers_contact c
                ON  c.id = s.contact_id
            ORDER BY s.start_date""")

    # Go four years into the future, but no further.
    nowish = now()
    max_month_index = (nowish.year + 4) * 12 + (nowish.month - 1)
    min_month_index = 1990 * 12 + 5

    contacts_with_sub = set()
    months = defaultdict(lambda: {
        'current': set(),
        'new': set(),
        'renew': set(),
        'expiring': set(),
    })
    for sub in subs:
        start_index = max(min_month_index,
            int(sub.start_year * 12 + sub.start_month - 1))
        while sub.contact_id in months[start_index]['current'] and \
                start_index < max_month_index:
            start_index += 1

        if start_index >= max_month_index:
            continue
        if sub.number_of_issues == -1:
            number_of_issues = 2 + max_month_index - start_index
        else:
            number_of_issues = int(int(sub.number_of_issues) / (sub.copies or 1))

        if sub.contact_id in contacts_with_sub:
            months[start_index]['renew'].add(sub.contact_id)
            if sub.contact_id in months[start_index]['expiring']:
                months[start_index]['expiring'].remove(sub.contact_id)
        else:
            months[start_index]['new'].add(sub.contact_id)
            contacts_with_sub.add(sub.contact_id)

        i = 0
        for i in range(number_of_issues):
            if start_index + i > max_month_index:
                break
            months[start_index + i]['current'].add(sub.contact_id)
        else:
            months[start_index + i]['expiring'].add(sub.contact_id)

    if request.GET.get('start_index') and request.GET.get('key'):
        disp_key = request.GET.get("key")
        start_index = int(request.GET['start_index'])
        try:
            ids = months[start_index][disp_key]
        except (KeyError, ValueError):
            raise Http404

        ids = sorted(list(ids))

        if request.GET.get('xlsx') or request.GET.get('pdf'):
            contacts = Contact.objects.filter(id__in=ids).order_by()
            if request.GET.get('xlsx'):
                return contact_xlsx_response(list(contacts))
            elif request.GET.get('pdf'):
                return contact_pdf_response(list(contacts))

        # Paginate on IDs rather than results, for performance -- id__in for
        # the whole list ends up being a 30-40 second query.  
        page = _paginate(request, ids)
        # ... then replace our results.
        page.object_list = list(Contact.objects.filter(id__in=page.object_list))
        disp_month = '%s-%02i' % (start_index / 12, (start_index % 12) + 1)
    else:
        page = None
        disp_key = None
        disp_month = None

    for index, details in months.items():
        year = int(index / 12)
        month = (index % 12) + 1
        details['label'] = "{}-{:02d}".format(year, month)
        for key in ('current', 'new', 'renew', 'expiring'):
            details[key] = len(details[key])

    return render(request, "stats/buckets.html", {
        'months': sorted(list(months.items()), reverse=True),
        'page': page,
        'disp_key': disp_key,
        'start_index': request.GET.get("start_index") or "",
        'disp_month': disp_month,
    })

@login_required
def stats_questions(request):
    return render(request, "stats/questions.html", {
        "received_issue": stats_forms.ReceivedIssue(prefix="received_issue"),
        "paid_subscription": stats_forms.PaidSubscription(prefix="paid_subscription"),
        "delivery_attempt": stats_forms.DeliveryAttempt(prefix="delivery_attempt"),
        "lived_in": stats_forms.LivedIn(prefix="lived_in"),
        "subscription_expired": stats_forms.SubscriptionExpired(prefix="subscription_expired"),
    })


def stats_answers(request):
    key = request.GET.get("key")
    context = {
       'key': key,
       'SUBSCRIPTION_CUTOFF': parse_datetime(settings.SUBSCRIPTION_CUTOFF)
    }

    form = None

    if key == "received_issue":
        form = stats_forms.ReceivedIssue(request.GET, prefix="received_issue")
    elif key == "paid_subscription":
        form = stats_forms.PaidSubscription(request.GET, prefix="paid_subscription")
    elif key == "delivery_attempt":
        form = stats_forms.DeliveryAttempt(request.GET, prefix="delivery_attempt")
    elif key == "lived_in":
        form = stats_forms.LivedIn(request.GET, prefix="lived_in")
    elif key == "subscription_expired":
        form = stats_forms.SubscriptionExpired(request.GET, prefix="subscription_expired")
    elif key == "contact_source_counts":
        counts = defaultdict(lambda: {})
        for source in ContactSource.objects.select_related('category').all():
            counts[source.category.name][source.name] = {
                "count": source.contact_set.count(),
                "pk": source.pk
            }
        counts = [[k, list(v.items())] for k,v in counts.items()]
        for category, source_counts in counts:
            source_counts.sort(key=lambda source_count: -source_count[1]['count'])
        counts.sort(key=lambda c: -sum([v['count'] for k,v in c[1]]))
        context['counts'] = counts
    elif key == "subscription_source_counts":
        counts = defaultdict()
        for source in SubscriptionSource.objects.all():
            counts[source.name] = {
                "count": source.subscription_set.count(),
                "pk": source.pk
            }
        context['counts'] = sorted(counts.items(), key=lambda c: -c[1]['count'])
    else:
        raise Http404

    if form is not None:
        if form.is_valid():
            context['cleaned_data'] = form.cleaned_data
            contact_ids = sorted(list(form.get_contact_id_set()))

            if request.GET.get('xlsx'):
                contacts = list(Contact.objects.filter(id__in=contact_ids))
                return contact_xlsx_response(contacts)
            if request.GET.get('pdf'):
                contacts = list(Contact.objects.filter(id__in=contact_ids))
                return contact_pdf_response(contacts)

            context['page'] = _paginate(request, contact_ids)
            context['page'].object_list = Contact.objects.filter(
                    id__in=context['page'].object_list)
        else:
            context['error'] = form.errors

    if 'cleaned_data' in context:
        if context['cleaned_data'].get('prison'):
            context['location'] = context['cleaned_data']['prison']
        elif context['cleaned_data'].get('city'):
            context['location'] = context['cleaned_data']['city']
            if 'state' in context['cleaned_data']:
                context['location'] += ", " + context['cleaned_data']['state']
        elif context['cleaned_data'].get('state'):
            context['location'] = context['cleaned_data']['state']
        else:
            context['location'] = "any state"

    return render(request, "stats/answers.html", context)

@login_required
def stats_map(request):
    return HttpResponse("Under Construction")
    #XXX Recode / verify / remove.
    # Map of where folks are from
    contacts = Contact.objects.all()
    _update_default_date()
    form = forms.ContactSearch(request.GET or CONTACT_SEARCH_DEFAULTS)
    if form.is_valid():
        contacts = form.constrain(contacts)

    state_counts = defaultdict(int)

    when = form.cleaned_data.get('search_addresses_current_on_date')
    if when:
        cads = ContactAddress.objects.filter(
                Q(end_date__isnull=True) | Q(end_date__gte=when),
                start_date__lte=when,
        )
    else:
        cads = ContactAddress.objects.all()

    # Get the counts for those that have prison addresses.
    pad_havers = contacts.filter(
            contactaddress__end_date__isnull=True, 
            contactaddress__prison__isnull=False).values_list('id', flat=True)
    for state in cads.select_related('address').filter(
            contact_id__in=pad_havers).distinct().values_list('state', flat=True):
        state_counts[state] += 1
    # Get the counts for those that have non-prison addresses.
    cad_havers = contacts.filter(
            contactaddress__end_date__isnull=True, 
            contactaddress__prison__isnull=True).values_list('id', flat=True)
    for contact in contacts.select_related('address').filter(
            contact_id__in=cad_havers).distinct().values_list('state', flat=True):
        if state:
            state_counts[state] += 1
        else:
            state_counts['international'] += 1

    # Pagination of all contacts.
    page = _paginate(request, contacts)
    return render(request, "stats/map.html", {
        'form': form,
        'state_counts': json.dumps(state_counts),
        'page': page,
    })


@login_required
def stats_subscriptions(request):
    return HttpResponse("Under Construction")
    #XXX Recode to incorporate subscription number_of_issues
    contact_types = Contact.objects.order_by().values_list('type', flat=True).distinct()
    sub_types = Subscription.objects.order_by().values_list('type', flat=True).distinct()
    out = {
        'label': [],
        'color': [
            #'rgb(47, 48, 48)', 'rgb(67, 68, 68, 1.0)',
            '#2f3030', '#434444',
            #'rgb(10, 99, 61)', 'rgb(40, 129, 91, 1.0)',
            '#0a633d', '#28815B',
            #'rgb(21, 202, 177)', 'rgb(51, 232, 207, 1.0)',
            '#15cab1', '#33e8cf',
            #'rgb(250, 159, 41)', 'rgb(255, 189, 71, 1.0)',
            '#fa9f29', '#ffbd47',
            #'rgb(120, 16, 17)', 'rgb(150, 46, 47, 1.0)',
            '#781011', '#962e2f',
        ],
        'values': [],
    }
    type_index = defaultdict(dict)
    type_count = 0
    for ctype in contact_types:
        for stype in sub_types:
            type_index[ctype][stype] = type_count
            type_count += 1
            out['label'].append("%s: %s" % (ctype, stype))
    
    # Get start and end, truncated to month.
    start = Issue.objects.order_by('date')[0].date
    start = datetime.datetime(start.year, start.month, 1, 0, 0, 0, 0, utc)
    end = now()
    end = datetime.datetime(end.year, end.month, 1, 0, 0, 0, 0, utc)

    time_cutoffs = {}
    cur = start
    label_count = 0
    while cur < end:
        cur = datetime.datetime(cur.year, cur.month, 1, 0, 0, 0, 0, utc)
        label = "%s-%s" % (cur.year, cur.month)
        #if label_count % 4 != 0:
        #    label = "."
        label_count += 1
        values_list = [0 for i in range(type_count)]
        out['values'].append({
            'label': label,
            'values': values_list
        })
        time_cutoffs[cur] = values_list
        cur += datetime.timedelta(days=31)

    # Massively more performant to do this raw query.
    subs = Subscription.objects.raw("""
        SELECT s.id,
               date_trunc('month', s.start_date) AS start_date,
               date_trunc('month', coalesce(s.end_date, current_date)) AS end_date,
               s.type AS type,
               c.type AS contact_type
           FROM subscribers_subscription s
           LEFT OUTER JOIN subscribers_contact c ON (s.contact_id = c.id)
    """)

    for sub in subs:
        sub_start = max(sub.start_date, start)
        sub_end = min(sub.end_date, end)
        cur = sub_start
        while cur < sub_end:
            sub_type_index = type_index[sub.contact_type][sub.type]
            try:
                time_cutoffs[cur][sub_type_index] += 1
            except IndexError:
                print(type_index, sub.contact_type, sub.type)
                print(time_cutoffs[cur])
                raise
                
            # Add next month, and normalize to beginning.
            cur += datetime.timedelta(days=31)
            cur = datetime.datetime(cur.year, cur.month, 1, 0, 0, 0, 0, utc)

    return render(request, "stats/subscriptions.html", {
        'data': json.dumps(out)
    })

@login_required
def sharingbatch_count(request):
    partner = get_object_or_404(SharingPartner, pk=request.GET.get("partner_id"))
    response = HttpResponse(json.dumps({
        'count': Contact.objects.shareable_with(partner).count()
    }))
    response['Content-Type'] = 'application/json'
    return response

@login_required
def download_sharingbatch(request, sharingbatch_id, filetype):
    batch = get_object_or_404(SharingBatch, pk=sharingbatch_id)
    if filetype == "xlsx":
        return contact_xlsx_response(batch.contacts.all(), fmt="blackstone")
    elif filetype == "pdf":
        return contact_pdf_response(batch.contacts.all())
    else:
        raise Http404

@login_required
def delete_sharingbatch(request, sharingbatch_id):
    if not request.user.has_perm("subscribers.delete_sharingbatch"):
        raise PermissionDenied
    batch = get_object_or_404(SharingBatch, pk=sharingbatch_id)
    batch.delete()
    return redirect("subscribers.sharing")

@login_required
def sharing(request):
    form = forms.SharingBatchForm(request.POST or None)
    if form.is_valid():
        contacts = list(Contact.objects.shareable_with(form.cleaned_data['partner']))
        if len(contacts) > 0:
            batch = SharingBatch()
            batch.partner = form.cleaned_data['partner']
            batch.save()
            batch.contacts.set(contacts)
            return redirect(request.path)
        else:
            form.errors['partner'] = [
                "There are no shareable contacts we haven't yet shared with this partner."
            ]

    return render(request, "sharing/share.html", {
        'form': form,
        'batches': SharingBatch.objects.all()
    })


@login_required
def import_subscriber_spreadsheet(request):
    """
    Import an xlsx spreadsheet as exported from the PLN website
    """

    def _parse_name(entry):
        tokens = re.split("[^\w-]", entry["Name"])
        number_parts = []
        name_parts = []
        for token in tokens:
            if re.search("\d+", token):
                number_parts.append(token)
            else:
                name_parts.append(token)
        number = " ".join(number_parts)
        if len(name_parts) == 1:
            return "", "", name_parts[0], number
        elif len(name_parts) == 2:
            return name_parts[0], "", name_parts[1], number
        elif len(name_parts) == 3:
            return name_parts[0], name_parts[1], name_parts[2], number
        else:
            return " ".join(name_parts[0:-1]), "", name_parts[-1], number

    def _parse_type(entry):
        if "Non-Incarcerated" in entry["Product"]:
            return "individual"
        if "Incarcerated" in entry["Product"]:
            return "prisoner"
        return "individual"

    def _parse_source(entry):
        # Same source for everyone
        return SubscriptionSource.objects.get_or_create(name="Web sale")[0]

    def _parse_copies(entry):
        return entry["Quantity"]

    def _parse_gender(entry):
        # All unknown
        return ""

    if request.method == "POST":
        # Read the spreadsheet into dict entries
        wb = load_workbook(request.FILES["file"])
        ws = wb.active
        itr = ws.iter_rows()
        heading = []
        for cell in next(itr):
            heading.append(str(cell.value))
        entries = []
        for row in itr:
            entries.append(dict(zip(heading, [cell.value for cell in row])))

        # Translate dict entries into model params
        sub_details = []
        for entry in entries:
            source_order_id = entry["Order ID"]
            source_order_item_id=entry["Order Item ID"]
            first, middle, last, number = _parse_name(entry)
            type_ = _parse_type(entry)
            source = _parse_source(entry)
            copies = _parse_copies(entry)
            gender = _parse_gender(entry)
            sub_details.append({
                "contact": {
                    "first_name": first,
                    "middle_name": middle,
                    "last_name": last,
                    "gender": gender,
                    "copies": copies,
                    "type": type_,
                },
                "address": dict(
                    address1=entry["Address"],
                    address2=entry["Address2"] or "",
                    city=entry["City"],
                    state=entry["State"],
                    zip=entry["Zip"],
                    country=entry["Country"] if entry["Country"] != "US" else "",
                ),
                "contact_address": {
                    "prisoner_number": number,
                },
                "subscription": {
                    "type": "subscription",
                    "number_of_issues": entry["Number of Issues"],
                    "source_order_id": entry["Order ID"],
                    "source_order_item_id": entry["Order Item ID"],
                    "source_order_product_name": entry["Product"],
                    "source": source,
                }
            })

        # Write subscriptions from model params
        existing_subscriptions = []
        new_subscriptions = []
        with transaction.atomic():
            for sub_detail in sub_details:
                order_id = sub_detail["subscription"]["source_order_id"]
                order_item_id = sub_detail["subscription"]["source_order_item_id"]
                sub = Subscription.objects.filter(
                    source_order_id=order_id,
                    source_order_item_id=order_item_id,
                ).first()
                if sub:
                    existing_subscriptions.append(sub)
                    continue

                contact, created = Contact.objects.get_or_create(**sub_detail["contact"])
                address, created = Address.objects.get_or_create(**sub_detail["address"])
                contact_address, created = ContactAddress.objects.get_or_create(
                    address=address,
                    contact=contact,
                    **sub_detail["contact_address"],
                )
                subscription, created = Subscription.objects.get_or_create(
                    contact=contact,
                    payer=contact,
                    **sub_detail["subscription"],
                )
                new_subscriptions.append(subscription)
        return render(
            request,
            "import_success.html",
            {
                "new_subscriptions": new_subscriptions,
                "existing_subscriptions": existing_subscriptions
            }
        )
    return render(request, "import_start.html")

                


    


#
# Upgrading
#

@login_required
def upgrade(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    form = forms.ConfirmPasswordForm(request.user, request.POST or None)
    if form.is_valid():
        stdout = tempfile.TemporaryFile()
        stderr = tempfile.TemporaryFile()
        error = False
        try:
            call_command("backup", stdout=stdout, stderr=stderr)
        except Exception as e:
            message = "Backup failed; upgrade not attempted: %s; %s" % (
                    e, traceback.format_exc())
            error = True
        else:
            try:
                call_command("update", stdout=stdout, stderr=stderr)
            except Exception as e:
                message = "Upgrade failed: %s; %s" % (e, traceback.format_exc())
                error = True
            else:
                message = "Backup and update completed successfully."
        stdout.flush()
        stderr.flush()
        stdout.seek(0)
        stderr.seek(0)
        stdout_val = stdout.read()
        stderr_val = stderr.read()
        stdout.close()
        stderr.close()
        return render(request, "management/upgrade_done.html", {
            "error": error,
            "message": message,
            "stdout": stdout_val,
            "stderr": stderr_val,
        })
    return render(request, "management/upgrade.html", {
        "form": form
    })

@login_required
def restart(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    if request.method == 'POST':
        request.session['do_restart'] = True
        return redirect("restart_do")
    return render(request, "management/restart.html")

@login_required
def do_restart(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    if not request.session.pop("do_restart", False):
        if request.session.pop("did_restart", True):
            return render(request, "management/restart_done.html")
        return redirect("restart")
    request.session['did_restart'] = True
    request.session.save()
    # We're going down!!
    import os
    os.abort()
    return HttpRespnse("Restarting... refresh the page in 60 seconds.")
