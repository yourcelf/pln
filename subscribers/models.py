import re
import datetime
from django.contrib.auth.models import User
from django.urls import reverse
from django.conf import settings
from django.db import models
from django.db.models import Q, Max, F
from django.utils.timezone import now, utc

from localflavor.us.models import USPostalCodeField

from .conceptq import concept

EXPIRATION_NOTICE_TYPES = {0: 'Expired', 1: 'Last Chance', 2: 'Early Renew'}

class Address(models.Model):
    """
    Mailing addresses of individuals, entities, or prisons.
    """
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    state = USPostalCodeField(blank=True)
    zip = models.CharField(max_length=10, blank=True,
            help_text="Zip (US), or Postal code (int'l)")
    region = models.CharField(max_length=255, blank=True,
            help_text="Province/Region (Non-US only)")
    country = models.CharField(max_length=255, blank=True,
            help_text="Leave blank for USA.")

    def get_address_lines(self):
        lines = []
        for field in (self.address2, self.address1):
            if field:
                lines.append(field)
        if self.city and (self.state or self.region):
            lines.append("{0}, {1}  {2}".format(
                self.city, self.state or self.region, self.zip
            ))
        else:
            for thing in (self.city, self.region, self.zip):
                if thing:
                    lines.append(thing)
        if self.country:
            lines.append(self.country)
        return lines

    def format_address(self):
        return "\n".join(self.get_address_lines())
    
    def __str__(self):
        return ", ".join(self.get_address_lines())

    class Meta:
        verbose_name_plural = "Addresses"

class PrisonType(models.Model):
    """
    The prison type -- e.g. prison, work release, jail, halfway house
    """
    name = models.CharField(max_length=50,
            help_text="Type of prison -- e.g. prison, jail, halfway house")
    def __str__(self):
        return self.name

class PrisonAdminType(models.Model):
    """
    Prison administration type -- e.g. Bureau of Prisons, State prison, CCA
    """
    name = models.CharField(max_length=50,
            help_text="Name of organization that administers prisons "
                      "-- e.g. CCA, BOP")
    def __str__(self):
        return self.name

class Prison(models.Model):
    """
    Administrative, address, and other details about a prison.
    """
    name = models.CharField(max_length=50,
            help_text="The name of this institution")
    type = models.ForeignKey(PrisonType, blank=True, null=True,
            help_text="The type of institution -- e.g. prison, jail, "
                      "halfway house", on_delete=models.CASCADE)
    subject_of_litigation = models.BooleanField(default=False)
    monitoring = models.BooleanField(default=False)
    investigating = models.BooleanField(default=False)
    address = models.ForeignKey(Address, on_delete=models.CASCADE,
            help_text="Mailing address for people housed here.")
    admin_type = models.ForeignKey(PrisonAdminType, blank=True, null=True,
            help_text="Organization administering this prison (e.g. CCA, BOP).",
            on_delete=models.CASCADE)
    admin_name = models.CharField(max_length=50, blank=True,
            help_text="Name of institution administering this prison "
                      "(e.g. 'Salinas Valley State Prison' administers "
                      "'Salinas Valley State Prison -- A'.")
    admin_address = models.ForeignKey(Address, blank=True, null=True,
            help_text="Administration's address for this prison, "
                      "if different from the main address.",
            related_name="prison_admin_set", on_delete=models.CASCADE)
    admin_phone = models.CharField(max_length=20, blank=True)
    warden = models.CharField(max_length=50, blank=True)
    men = models.NullBooleanField()
    women = models.NullBooleanField()
    juveniles = models.NullBooleanField()
    minimum = models.NullBooleanField()
    medium = models.NullBooleanField()
    maximum = models.NullBooleanField()
    control_unit = models.NullBooleanField()
    death_row = models.NullBooleanField()

    def save(self, *args, **kwargs):
        super(Prison, self).save(*args, **kwargs)
        # Ensure that the denormalized address field in ContactAddress remains
        # synchronized to associated Prison addresses.
        ContactAddress.objects.filter(
            prison_id=self.pk
        ).update(address=self.address)

    def format_list_display(self):
        if self.admin_type:
            name = "%s (%s)" % (self.name, self.admin_type)
        else:
            name = self.name
        return "\n".join((name, self.address.format_address()))

    def __str__(self):
        if self.admin_type is not None:
            return "%s: %s (%s)" % (self.name, self.type, self.admin_type.name)
        elif self.type is not None:
            return "%s: %s" % (self.name, self.type)
        else:
            return "%s" % (self.name)


class ContactManager(models.Manager):
    @concept
    def reachable(self, when=None):
        stays = list(MailingStay.objects.stayed().values_list(
            'contact_id', flat=True))
        # Undenormalized strategy:
        #has_address = Q(contactaddress__isnull=False,
        #                contactaddress__end_date__isnull=True) 
        # Denormalized:
        has_address = Q(last_address_date__gte=when or now())
        return Q(~Q(id__in=stays), has_address,
                 deceased=False, lost_contact=False)

    @concept
    def unreachable(self, when=None):
        stays = MailingStay.objects.stayed().values_list(
                'contact_id', flat=True)
        # Undenormalized strategy:
        #no_address = ~Q(contactaddress__end_date__isnull=True) | \
        #             Q(contactaddress__isnull=True)
        # Denormalized:
        no_address = ~Q(last_address_date__gte=when or now())
        return no_address | Q(id__in=stays) | \
               Q(deceased=True) | Q(lost_contact=True)

    def issue_needed(self, issue):
        issue_mailings = Mailing.objects.issue_exists(issue).values_list(
                'id', flat=True)
        return self.filter(
                self.reachable().q, ~Q(issues_remaining=0), copies__gt=0
            ).exclude(
                mailing__id__in=issue_mailings
            ).distinct()

    def issue_unreachable(self, issue):
        issue_mailings = Mailing.objects.issue_exists(issue).values_list(
                'id', flat=True)
        return self.filter(
                self.unreachable().q, ~Q(issues_remaining=0)
        ).exclude(
                mailing__id__in=issue_mailings
        ).distinct()

    def issue_enqueued(self, issue):
        mailings = Mailing.objects.issue_enqueued(issue).values_list(
                'id', flat=True)
        return self.filter(mailing__id__in=mailings).distinct()

    def issue_sent(self, issue):
        mailings = Mailing.objects.issue_sent(issue).values_list(
                'id', flat=True)
        return self.filter(mailing__id__in=mailings).distinct()

    #
    # Subscription expiration notices
    #

    @concept
    def _has_num_left_after_current(self, num, window=10, when=None):
        """
        Return contacts that have the number of specified issues remaining.  If
        the latest issue has not been sent, or has a date older than `window`
        days, anticipate a new issue mailing and return contacts with an
        additional issue remaining.
        """
        try:
            issue = Issue.objects.latest('date')
        except Issue.DoesNotExist:
            return Q(issues_remaining=num*F('copies'))

        when = when or now()
        current = Mailing.objects.filter(type__type='Issue', issue=issue,
                sent__isnull=False)
        if current.exists():
            issue_recipients = current.values_list('contact_id', flat=True)
            got_current = Q(id__in=issue_recipients)
            # Mailings for the latest issue are sent
            if issue.date > when:
                # The latest issue's date is ahead of now.
                issue_imminent = False
            elif when - issue.date < datetime.timedelta(days=window):
                # The latest issue's date is within <window> days of now.
                issue_imminent = False
            else:
                # The latest issue's date is older than <window> days, so
                # consider this to be in relation to the forthcoming issue.
                issue_imminent = True
        else:
            got_current = None
            # Mailings for the latest issue have not been sent. So expect
            # those to go out right away.
            issue_imminent = True

        if issue_imminent:
            remaining = num + 1
        else:
            remaining = num

        if got_current:
            return Q(got_current, issues_remaining=remaining * F('copies'))
        else:
            return Q(issues_remaining=remaining * F('copies'))

    def _has_num_left_after_current_via(self, num, rel, when=None):
        if rel == "recipient":
            return self._has_num_left_after_current(num, when=when).q
        else:
            return self._has_num_left_after_current(num, when=when).via(
                "subscriptions_payed_for__contact"
            ) & ~Q(subscriptions_payed_for__contact_id=F('id'))

    def _notice_needed_or_unreachable(self, remaining, rel, when=None):
        """
        """
        recent_mailings = Mailing.objects.notice_type_near(
            EXPIRATION_NOTICE_TYPES[remaining], when or now()
        ).values_list('id', flat=True)
        remaining_q = self._has_num_left_after_current_via(remaining, rel, when=when)
        return self.filter(remaining_q).exclude(
                    mailing__id__in=recent_mailings).distinct()

    def notice_needed(self, remaining, rel, when=None):
        """
        Return a queryset of reachable contacts who have `remaining`
        issues left (after the current issue), and who have not received a
        warning for this number remaining recently. `rel` is either "payer",
        which returns only those who are paying for others' subscriptions, or
        "recipient", which includes those who are receiving subscriptions
        (whether or not they pay).
        """
        return self._notice_needed_or_unreachable(remaining, rel, when=when).filter(
                self.reachable(when).q)

    def notice_unreachable(self, remaining, rel, when=None):
        """
        Return a queryset of unreachable contacts who have `remaining`
        issues left (after the current issue), and who have not received a
        warning for this number remaining recently. `rel` is either "payer",
        which returns only those who are paying for others' subscriptions, or
        "recipient", which includes those who are receiving subscriptions
        (whether or not they pay).
        """
        return self._notice_needed_or_unreachable(remaining, rel, when=when).filter(
                self.unreachable(when).q)

    def notice_enqueued(self, remaining, rel, when=None):
        """
        Return a queryset of all contacts who have a `remaining` issues left
        warning enqueued but not sent.
        """
        mailings = Mailing.objects.notice_type_near(
            EXPIRATION_NOTICE_TYPES[remaining], when or now(), sent=False
        ).values_list('id', flat=True)
        remaining_q = self._has_num_left_after_current_via(remaining, rel, when=when)
        return self.filter(remaining_q).filter(
                mailing__id__in=mailings).distinct()

    def notice_sent(self, remaining, rel, when=None):
        """
        Return a queryset of all contacts who have a `remaining` issues left
        warning sent recently.
        """
        mailings = Mailing.objects.notice_type_near(
            EXPIRATION_NOTICE_TYPES[remaining], when or now(), sent=True
        ).values_list('id', flat=True)
        remaining_q = self._has_num_left_after_current_via(remaining, rel, when=when)
        return self.filter(remaining_q).filter(
                mailing__id__in=mailings).distinct()

    #
    # Fuzzy search
    #

    def similar_to(self, first, middle, last, min_score=0):
        select = {}
        select_params = []
        order_by = []
        where = ""
        params = []
        fields = (
            (first, "first_name"),
            (middle, "middle_name"),
            (last, "last_name")
        )
        used_fields = [f for f in fields if f[0]]
        for name, key in used_fields:
            select['{0}_sim'.format(key)] = "similarity({0}, %s)".format(key)
            select_params.append(name)
            order_by.append('-{0}_sim'.format(key))
            if where:
                where += " OR "
            where += 'similarity({0}, %s) > %s'.format(key)
            params.append(name)
            params.append(min_score)
        order_by.append('last_name')
        return self.extra(
            select=select,
            select_params=select_params,
            order_by=order_by,
            where=[where],
            params=params,
        )
        
    def shareable_with(self, partner):
        # We can only share people who have opted out, and whom we learned
        # about via an info request.
        return self.filter(self.reachable().q,
                source__name='Info Request', opt_out=False,
            ).exclude(sharingbatch__partner=partner)

class ContactSourceCategory(models.Model):
    """
    General category for source info about contacts (e.g. Letter, Flyer,
    Internet, etc).
    """
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name

class ContactSource(models.Model):
    """
    Specific source for a contact (e.g. Web Sale, Book Order, Info Request)
    """
    name = models.CharField(max_length=255)
    category = models.ForeignKey(ContactSourceCategory, null=True, on_delete=models.CASCADE)
    def __str__(self):
        if self.category:
            return "%s: %s" % (self.category, self.name)
        return self.name
    class Meta:
        ordering = ['category__name', 'name']

class OrganizationType(models.Model):
    """
    Type of entity -- e.g. Library, University, Foundation
    """
    type = models.CharField(max_length=50)
    def __str__(self):
        return self.type

class Contact(models.Model):
    """
    Individuals, entities, and all other people with whom we're in contact.
    """
    MISSING_NAME = "[name missing]"

    first_name = models.CharField(max_length=255, blank=True, db_index=True,
            help_text="Individual, or contact within organization")
    middle_name = models.CharField(max_length=255, blank=True, db_index=True)
    last_name = models.CharField(max_length=255, blank=True, db_index=True)
    title = models.CharField(max_length=255, blank=True)
    organization_name = models.CharField(max_length=255, blank=True,
            help_text="Leave blank for individuals.", db_index=True)
    organization_type = models.ForeignKey(OrganizationType,
            blank=True, null=True, on_delete=models.CASCADE,
            help_text="Leave blank for individuals.")
    email = models.EmailField(blank=True)
    gender = models.CharField(max_length=1, choices=(
        ("", "Unknown"),
        ('M', "Male"),
        ('F', 'Female'),
        ('T', 'Transgender'),
    ), default="", blank=True)
    type = models.CharField(max_length=50, choices=(
        ('individual', 'Individual'),
        ('prisoner', 'Prisoner'),
        ('entity', 'Entity'),
        ('advertiser', 'Advertiser'),
        ('fop', 'Friend of PLN'),
    ))
    source = models.ForeignKey(ContactSource, blank=True, null=True,
            help_text="How did we find out about this contact? "
                      "Leave blank if unknown.", on_delete=models.CASCADE)
    opt_out = models.BooleanField(default=False,
            help_text="Opt out from appeals.")
    created = models.DateTimeField(default=now)
    deceased = models.BooleanField(default=False)  # KJ added default
    lost_contact = models.BooleanField(default=False,  # KJ added default
        help_text="Check this if the last known address "
                  "for this contact no longer works.")
    donor = models.BooleanField(default=False,  # KJ added default
            verbose_name="Is a donor",
            help_text="Check if this contact should be "
                      "included in donor appeals.")
    tags = models.ManyToManyField('Tag', help_text="List of tags",
            blank=True)
    copies = models.IntegerField(default=1,
            help_text="How many copies should be sent for each "
                      "subscription this contact has?")

    issue_adjustment = models.IntegerField(default=0,
            help_text="Adjustment for number of issues to correct for "
                      "old access database discrepancies")

    # Denormalized fields
    issues_remaining = models.IntegerField(default=0,
            help_text="Automatically calculated from subscriptions "
                      "(don't change -- your changes will be overwritten).")
    last_address_date = models.DateTimeField(
            blank=True, null=True, db_index=True,
            help_text="Automatically calculated from contact addresses "
                      "(don't change -- your changes will be overwritten).")

    objects = ContactManager()

    def show_name(self):
        return self.organization_name or self.get_full_name()

    def current_address(self):
        try:
            return self.contactaddress_set.select_related('address').get(
                    end_date__isnull=True)
        except ContactAddress.DoesNotExist:
            return None

    def first_subscribed(self):
        try:
            return self.subscription_set.order_by('start_date')[0].start_date
        except IndexError:
            return None

    def get_full_name(self):
        name = " ".join(
            n for n in (self.first_name, self.middle_name, self.last_name) if n
        )
        if self.title:
            name += ", %s" % self.title
        if name == "":
            name = self.MISSING_NAME
        return name

    def get_in_care_of(self):
        if self.organization_name:
            full_name = self.get_full_name()
            if full_name and full_name != self.MISSING_NAME:
                return "%s" % full_name
        return ""

    def format_address(self):
        cad = self.current_address()
        if cad:
            return "\n".join(cad.get_address_lines())
        else:
            return "\n".join((self.show_name(), "[No address on file]"))

    def get_absolute_url(self):
        return reverse("subscribers.contacts.show", args=[self.pk])

    def get_issues_remaining_string(self):
        if self.issues_remaining == -1:
            return "Unlimited"
        return "{} left".format(self.issues_remaining)

    def get_expiration_string(self):
        if self.issues_remaining == 0:
            return "None"
            # While distinguishing 'expired' from 'none' is nice, it kills
            # performance for bulk CSV/PDF queries with expiration strings.
            #if self.subscription_set.exists():
            #    return "Expired"
            #else:
            #    return "None"
        if self.issues_remaining == -1:
            return "Perpetual"
        start = now()
        year = start.year
        month = start.month - 1
        month += int(self.issues_remaining / self.copies)
        date = datetime.datetime(year + int(month/12), (month%12) + 1, 1)
        return date.strftime("%b %Y")

    def get_total_issues_bought(self):
        return sum(self.subscription_set.filter(
            number_of_issues__gt=0
        ).values_list('number_of_issues', flat=True)) + self.issue_adjustment

    def get_total_issues_sent_after_cutoff(self):
        return self.mailing_set.filter(
            type__type='Issue',
            sent__gte=settings.SUBSCRIPTION_CUTOFF
        ).count()

    def set_issues_remaining(self):
        if self.subscription_set.filter(number_of_issues=-1).exists():
            self.issues_remaining = -1
        else:
            self.issues_remaining = max(
                0, self.get_total_issues_bought() - self.get_total_issues_sent_after_cutoff()
            )
        self.save()

    def set_last_address_date(self):
        self.last_address_date = None
        if self.contactaddress_set.filter(end_date__isnull=True).exists():
            # Set to arbitrary high value.
            self.last_address_date = datetime.datetime(3000, 1, 1, 0, 0, 0, 0, utc)
        else:
            try:
                self.last_address_date = self.contactaddress_set.aggregate(
                    max_date=Max('end_date')
                )['max_date']
            except KeyError:
                pass
        self.save()

    def set_tag_list(self, tag_list):
        tag_names = Tag.split(tag_list)
        tags = []
        for name in tag_names:
            tags.append(Tag.objects.get_or_create(name=name)[0])
        self.tags = tags

    def tag_list(self):
        return self.tags.as_list()

    def __str__(self):
        return self.show_name()

    class Meta:
        ordering = ['last_name', 'organization_name']

class TagManager(models.Manager):
    def as_list(self):
        return ",".join(self.values_list('name', flat=True))

class Tag(models.Model):
    name = models.CharField(max_length=25)

    objects = TagManager()

    def save(self, *args, **kwargs):
        self.name = Tag.clean(self.name)
        super(Tag, self).save(*args, **kwargs)

    @classmethod
    def clean(self, val):
        return re.sub("[^a-zA-Z0-9. ]", "", val.lower().strip())

    @classmethod
    def split(self, val):
        return [Tag.clean(a) for a in val.split(",")]

    def __str__(self):
        return self.name

class ContactAddress(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=now, db_index=True)
    end_date = models.DateTimeField(blank=True, null=True, db_index=True)
    address = models.ForeignKey('Address', on_delete=models.CASCADE)
    # Prison-specific fields -- may be blank/null.
    prison = models.ForeignKey('Prison', blank=True, null=True, on_delete=models.CASCADE)
    prisoner_number = models.CharField(max_length=50, blank=True)
    unit = models.CharField(max_length=255, blank=True)
    death_row = models.NullBooleanField()
    control_unit = models.NullBooleanField()

    def save(self, *args, **kwargs):
        if self.prison:
            self.address_id = self.prison.address_id
        super(ContactAddress, self).save(*args, **kwargs)
        self.contact.set_last_address_date()

    def delete(self, *args, **kwargs):
        contact = self.contact
        super(ContactAddress, self).delete(*args, **kwargs)
        self.contact.set_last_address_date()

    def contact_name(self):
        return self.contact.show_name()

    def is_current(self):
        n = now()
        return self.start_date <= n and (
            (self.end_date is None) or (self.end_date > n)
        )

    def get_address_lines(self):
        lines = []
        if self.prisoner_number:
            lines.append("{0}, #{1}".format(
                self.contact.show_name(), self.prisoner_number.lstrip('#'))
            )
        else:
            careof = self.contact.get_in_care_of()
            if careof:
                lines.append(careof)
            lines.append(self.contact.show_name())
        if self.contact.type == 'prisoner' and self.prison_id:
            lines.append(self.prison.name)
        if self.unit:
            lines.append(self.unit)
        lines += self.address.get_address_lines()
        return lines

    def format_address(self):
        return "\n".join(self.get_address_lines())

    def __str__(self):
        return ", ".join((
            str(self.contact), 
            str(self.start_date),
            str(self.end_date)))

    class Meta:
        verbose_name_plural = "Contact addresses"
        ordering = ['-end_date', 'contact']
    

class MailingStayManager(models.Manager):
    @concept
    def not_stayed(self):
        return Q(end_date__isnull=False, end_date__lte=now())

    @concept
    def stayed(self):
        return Q(Q(end_date__gte=now()) | Q(end_date__isnull=True),
                 start_date__lte=now())

class MailingStay(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    start_date = models.DateTimeField(default=now)
    end_date = models.DateTimeField(blank=True, null=True,
            help_text="Leave blank to withold mail until further notice.")

    objects = MailingStayManager()
    
    class Meta:
        ordering = ['-end_date']

    def __str__(self):
        return ", ".join((str(self.contact), str(self.start_date),
                          str(self.end_date)))

class InquiryResponseType(models.Model):
    description = models.CharField(max_length=255)
    code = models.CharField(max_length=255, blank=True)
    generate_mailing = models.BooleanField(default=False)  # KJ added default
    def __str__(self):
        return self.description

class InquiryType(models.Model):
    description = models.CharField(max_length=255)
    def __str__(self):
        return self.description

class InquiryManager(models.Manager):
    def reverse_chronological(self):
        return self.order_by('-date')

    def mailing_needed(self):
        return self.filter(
               Contact.objects.reachable().via('contact')
           ).filter(
               response_type__generate_mailing=True, mailing__isnull=True
           )

    def mailing_unreachable(self):
        return self.filter(
               response_type__generate_mailing=True, mailing__isnull=True
            ).filter(
               Contact.objects.unreachable().via('contact')
            )

    def mailing_enqueued(self):
        return self.filter(response_type__generate_mailing=True,
                 mailing__isnull=False, mailing__sent__isnull=True)

    def mailing_sent(self):
        return self.filter(response_type__generate_mailing=True,
                 mailing__isnull=False, mailing__sent__isnull=False)

class Inquiry(models.Model):
    contact = models.ForeignKey('Contact', on_delete=models.CASCADE)
    request_type = models.ForeignKey(InquiryType, on_delete=models.CASCADE)
    response_type = models.ForeignKey(InquiryResponseType, on_delete=models.CASCADE)
    date = models.DateTimeField(default=now)

    objects = InquiryManager()

    def __str__(self):
        return ": ".join((str(self.response_type), str(self.contact)))

    class Meta:
        ordering = ['date']
        verbose_name_plural = "Inquiries"

class DonorBatch(models.Model):
    date = models.DateTimeField(default=now)
    contacts = models.ManyToManyField('Contact')
    def __str__(self):
        return "%s: %s contacts" % (self.date, self.contacts.count())
    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'Donor batches'

class Note(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    author = models.ForeignKey(User, related_name="notes_authored", on_delete=models.CASCADE)
    text = models.TextField()
    created = models.DateTimeField(default=now)
    modified = models.DateTimeField()

    def contact_name(self):
        return self.contact.show_name()

    def save(self, *args, **kwargs):
        self.modified = now()
        super(Note, self).save()

    def __str__(self):
        return "%s: %s" % (self.contact.show_name(), self.text[:15])

    class Meta:
        ordering = ['-created']

class SubscriptionSource(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name

class SubscriptionStopReason(models.Model):
    reason = models.CharField(max_length=50)
    def __str__(self):
        return self.reason

class SubscriptionManager(models.Manager):

    @concept
    def not_self_payer(self):
        return Q(contact__id__lt=F('payer__id')) | \
               Q(contact__id__gt=F('payer__id'))

    @concept
    def self_payer(self):
        return Q(contact__id=F('payer__id'))

    @concept
    def perpetual(self):
        return Q(end_date__isnull=True)

class Subscription(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    payer = models.ForeignKey(Contact, related_name="subscriptions_payed_for", on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=(
        ('subscription', 'Subscription'),
        ('trial', 'Trial'),
    ))
    source = models.ForeignKey(SubscriptionSource, blank=True, null=True,
            help_text="Leave blank if unknown.", on_delete=models.CASCADE)
    source_order_id = models.PositiveIntegerField(blank=True,  null=True)
    source_order_item_id = models.PositiveIntegerField(blank=True, null=True)
    source_order_product_name = models.CharField(max_length=255, blank=True, null=True)
    paid_amount = models.IntegerField(default=0)
    start_date = models.DateTimeField(default=now, db_index=True)
    end_date = models.DateTimeField(blank=True, null=True, db_index=True,
        help_text="Historical field -- don't change")
    number_of_issues = models.IntegerField(default=0)
    cancelled_issues = models.IntegerField(default=0)

    stop_reason = models.ForeignKey(SubscriptionStopReason,
        blank=True, null=True, on_delete=models.CASCADE)

    objects = SubscriptionManager()

    def save(self, *args, **kwargs):
        super(Subscription, self).save(*args, **kwargs)
        self.contact.set_issues_remaining()

    def delete(self, *args, **kwargs):
        contact = self.contact
        super(Subscription, self).delete(*args, **kwargs)
        contact.set_issues_remaining()

    def end_early(self, reason, when=None):
        self.stop_reason = reason
        self.end_date = when or now()
        if self.number_of_issues == -1:
            # Cancel a perpetual subscription.  Set the balance,
            # "number_of_issues", to whatever is needed to make a zero total
            # balance.
            self.cancelled_issues = -1
            # Calculate the difference between sent and bought.
            bought = self.contact.get_total_issues_bought()
            sent = self.contact.get_total_issues_sent_after_cutoff()
            balance = max(0, sent - bought)
            self.number_of_issues = balance
        else:
            # Number of cancelled issues for a non-perpetual is the smaller of
            # the number of issues remaining and the number of issues from this
            # subscription.
            if self.contact.issues_remaining == -1:
                self.cancelled_issues = self.number_of_issues
            else:
                self.cancelled_issues = min(
                    self.contact.issues_remaining, self.number_of_issues
                )
            self.number_of_issues -= self.cancelled_issues
        self.save()

    def restart(self):
        if self.cancelled_issues == -1:
            # Restart a cancelled perpetual subscription.
            self.number_of_issues = -1
        else:
            # Restart a cancelled normal subscription.
            self.number_of_issues += self.cancelled_issues
        self.cancelled_issues = 0
        self.end_date = None
        self.stop_reason = None
        self.save()

    def contact_name(self):
        return self.contact.show_name()

    def __str__(self):
        return "%s: %s, %s" % (self.contact, self.type, self.end_date)

    class Meta:
        ordering = ['-end_date', '-start_date']
        unique_together = [["source_order_id", "source_order_item_id"]]

class Issue(models.Model):
    volume = models.IntegerField()
    number = models.IntegerField()
    date = models.DateTimeField()

    def num_recipients(self):
        return self.mailing_set.filter(type__type='Issue', issue=self).count()

    def num_censored_recipients(self):
        return self.mailing_set.filter(censored=True).count()

    def __str__(self):
        return "Volume %s Number %s" % (self.volume, self.number)

    class Meta:
        ordering = ['-date']

class MailingLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.ForeignKey('MailingType', on_delete=models.CASCADE)
    note = models.CharField(max_length=50, blank=True, default="",
        help_text="Additional info identifying this mailing log.")
    created = models.DateTimeField(default=now)
    mailings = models.ManyToManyField('Mailing')

    def save(self, *args, **kwargs):
        update_children = False
        if self.pk:
            orig = MailingLog.objects.get(pk=self.pk)
            if orig.type != self.type:
                update_children = True
        super(MailingLog, self).save(*args, **kwargs)
        if update_children:
            for mailing in self.mailings.all():
                mailing.type = self.type
                mailing.save()

    def __str__(self):
        return "%s: %s" % (self.user, self.created)

    def get_absolute_url(self):
        return reverse("subscribers.mailinglogs.show", args=[self.pk])

class MailingType(models.Model):
    type = models.CharField(max_length=50)
    def __str__(self):
        return self.type

class MailingManager(models.Manager):
    @concept
    def issue_exists(self, issue):
        return Q(issue=issue, type__type="Issue")

    @concept
    def issue_enqueued(self, issue):
        return Q(issue=issue, type__type="Issue", sent__isnull=True)

    @concept
    def issue_sent(self, issue):
        return Q(issue=issue, type__type="Issue", sent__isnull=False)

    @concept
    def notice_type_near(self, mtype, when, slop=40, sent=None):
        if sent == True:
            sent_q = Q(sent__isnull=False)
        elif sent == False:
            sent_q = Q(sent__isnull=True)
        else:
            sent_q = Q()
        if isinstance(mtype, str):
            type_q = Q(type__type=mtype)
        else:
            type_q = Q(type=mtype)
        return Q(sent_q, type_q,
                 created__gte=when - datetime.timedelta(days=slop),
                 created__lte=when + datetime.timedelta(days=slop))

    @concept
    def inquiry_enqueued(self, inquiry):
        return Q(inquiry=inquiry, sent__isnull=True)

    @concept
    def inquiry_sent(self, inquiry):
        return Q(inquiry=inquiry, sent__isnull=False)

class Mailing(models.Model):
    type = models.ForeignKey(MailingType, blank=True, null=True, db_index=True, on_delete=models.CASCADE)
    issue = models.ForeignKey(Issue, blank=True, null=True, db_index=True, on_delete=models.CASCADE)
    inquiry = models.ForeignKey(Inquiry, blank=True, null=True, on_delete=models.CASCADE)
    custom_text = models.TextField(blank=True)
    contact = models.ForeignKey(Contact, db_index=True, on_delete=models.CASCADE)
    censored = models.BooleanField(
        default=False,
        help_text="Check if this mailing was refused delivery due to censorship."
    )
    notes = models.TextField(
        help_text="Record any unusual details or special info about this mailing here"
    )
    created = models.DateTimeField(default=now)
    sent = models.DateTimeField(blank=True, null=True, db_index=True)

    objects = MailingManager()

    def save(self, *args, **kwargs):
        super(Mailing, self).save(*args, **kwargs)
        if self.issue_id:
            self.contact.set_issues_remaining()
            
    def delete(self, *args, **kwargs):
        contact = self.contact
        super(Mailing, self).delete(*args, **kwargs)
        contact.set_issues_remaining()

    def contact_name(self):
        return self.contact.show_name()

    def __str__(self):
        return "%s: %s, %s" % (self.contact, self.type, 
                self.sent.strftime("%Y-%m-%d") if self.sent else "Unsent")
    class Meta:
        ordering = ['-created', 'contact__last_name']

class SharingPartner(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class SharingBatch(models.Model):
    partner = models.ForeignKey(SharingPartner, on_delete=models.CASCADE)
    contacts = models.ManyToManyField(Contact)
    date = models.DateTimeField(default=now)

    class Meta:
        ordering = ['-date']
        verbose_name_plural = 'Sharing batches'

    def __str__(self):
        return str(self.date)

class OtherPurchaseType(models.Model):
    type = models.CharField(max_length=255)

    class Meta:
        ordering = ['type']

    def __str__(self):
        return str(self.type)

class OtherPurchase(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    type = models.ForeignKey(OtherPurchaseType, on_delete=models.CASCADE)
    count = models.IntegerField(blank=True, null=True, help_text="How many items were purchased?")
    total_cost = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    date = models.DateTimeField(default=now)

    class Meta:
        ordering = ['date']

    def __str__(self):
        return "{}, {}, {}".format(self.contact, self.type, self.date)
