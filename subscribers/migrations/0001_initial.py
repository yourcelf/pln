# -*- coding: utf-8 -*-


from django.db import migrations, models
import localflavor.us.models
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address1', models.CharField(max_length=255)),
                ('address2', models.CharField(max_length=255, blank=True)),
                ('city', models.CharField(max_length=255)),
                ('state', localflavor.us.models.USPostalCodeField(blank=True, max_length=2, choices=[(b'AL', 'Alabama'), (b'AK', 'Alaska'), (b'AS', 'American Samoa'), (b'AZ', 'Arizona'), (b'AR', 'Arkansas'), (b'AA', 'Armed Forces Americas'), (b'AE', 'Armed Forces Europe'), (b'AP', 'Armed Forces Pacific'), (b'CA', 'California'), (b'CO', 'Colorado'), (b'CT', 'Connecticut'), (b'DE', 'Delaware'), (b'DC', 'District of Columbia'), (b'FM', 'Federated States of Micronesia'), (b'FL', 'Florida'), (b'GA', 'Georgia'), (b'GU', 'Guam'), (b'HI', 'Hawaii'), (b'ID', 'Idaho'), (b'IL', 'Illinois'), (b'IN', 'Indiana'), (b'IA', 'Iowa'), (b'KS', 'Kansas'), (b'KY', 'Kentucky'), (b'LA', 'Louisiana'), (b'ME', 'Maine'), (b'MH', 'Marshall Islands'), (b'MD', 'Maryland'), (b'MA', 'Massachusetts'), (b'MI', 'Michigan'), (b'MN', 'Minnesota'), (b'MS', 'Mississippi'), (b'MO', 'Missouri'), (b'MT', 'Montana'), (b'NE', 'Nebraska'), (b'NV', 'Nevada'), (b'NH', 'New Hampshire'), (b'NJ', 'New Jersey'), (b'NM', 'New Mexico'), (b'NY', 'New York'), (b'NC', 'North Carolina'), (b'ND', 'North Dakota'), (b'MP', 'Northern Mariana Islands'), (b'OH', 'Ohio'), (b'OK', 'Oklahoma'), (b'OR', 'Oregon'), (b'PW', 'Palau'), (b'PA', 'Pennsylvania'), (b'PR', 'Puerto Rico'), (b'RI', 'Rhode Island'), (b'SC', 'South Carolina'), (b'SD', 'South Dakota'), (b'TN', 'Tennessee'), (b'TX', 'Texas'), (b'UT', 'Utah'), (b'VT', 'Vermont'), (b'VI', 'Virgin Islands'), (b'VA', 'Virginia'), (b'WA', 'Washington'), (b'WV', 'West Virginia'), (b'WI', 'Wisconsin'), (b'WY', 'Wyoming')])),
                ('zip', models.CharField(help_text=b"Zip (US), or Postal code (int'l)", max_length=10, blank=True)),
                ('region', models.CharField(help_text='Province/Region (Non-US only)', max_length=255, blank=True)),
                ('country', models.CharField(help_text='Leave blank for USA.', max_length=255, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Addresses',
            },
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(help_text=b'Individual, or contact within organization', max_length=255, db_index=True, blank=True)),
                ('middle_name', models.CharField(db_index=True, max_length=255, blank=True)),
                ('last_name', models.CharField(db_index=True, max_length=255, blank=True)),
                ('title', models.CharField(max_length=255, blank=True)),
                ('organization_name', models.CharField(help_text=b'Leave blank for individuals.', max_length=255, db_index=True, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('gender', models.CharField(default=b'', max_length=1, blank=True, choices=[(b'', b'Unknown'), (b'M', b'Male'), (b'F', b'Female'), (b'T', b'Transgender')])),
                ('type', models.CharField(max_length=50, choices=[(b'individual', b'Individual'), (b'prisoner', b'Prisoner'), (b'entity', b'Entity'), (b'advertiser', b'Advertiser'), (b'fop', b'Friend of PLN')])),
                ('opt_out', models.BooleanField(default=False, help_text=b'Opt out from appeals.')),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('deceased', models.BooleanField(default=False)),
                ('lost_contact', models.BooleanField(default=False, help_text=b'Check this if the last known address for this contact no longer works.')),
                ('donor', models.BooleanField(default=False, help_text=b'Check if this contact should be included in donor appeals.', verbose_name=b'Is a donor')),
                ('copies', models.IntegerField(default=1, help_text=b'How many copies should be sent for each subscription this contact has?')),
                ('issue_adjustment', models.IntegerField(default=0, help_text=b'Adjustment for number of issues to correct for old access database discrepancies')),
                ('issues_remaining', models.IntegerField(default=0, help_text=b"Automatically calculated from subscriptions (don't change -- your changes will be overwritten).")),
                ('last_address_date', models.DateTimeField(help_text=b"Automatically calculated from contact addresses (don't change -- your changes will be overwritten).", null=True, db_index=True, blank=True)),
            ],
            options={
                'ordering': ['last_name', 'organization_name'],
            },
        ),
        migrations.CreateModel(
            name='ContactAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now, db_index=True)),
                ('end_date', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('prisoner_number', models.CharField(max_length=50, blank=True)),
                ('unit', models.CharField(max_length=255, blank=True)),
                ('death_row', models.NullBooleanField()),
                ('control_unit', models.NullBooleanField()),
                ('address', models.ForeignKey(to='subscribers.Address')),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-end_date', 'contact'],
                'verbose_name_plural': 'Contact addresses',
            },
        ),
        migrations.CreateModel(
            name='ContactSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ['category__name', 'name'],
            },
        ),
        migrations.CreateModel(
            name='ContactSourceCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='DonorBatch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('contacts', models.ManyToManyField(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='Inquiry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='InquiryResponseType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=255)),
                ('code', models.CharField(max_length=255, blank=True)),
                ('generate_mailing', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='InquiryType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('volume', models.IntegerField()),
                ('number', models.IntegerField()),
                ('date', models.DateTimeField()),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='Mailing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('custom_text', models.TextField(blank=True)),
                ('censored', models.BooleanField(default=False, help_text=b'Check if this mailing was refused delivery due to censorship.')),
                ('notes', models.TextField(help_text=b'Record any unusual details or special info about this mailing here')),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('sent', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
                ('inquiry', models.ForeignKey(blank=True, to='subscribers.Inquiry', null=True)),
                ('issue', models.ForeignKey(blank=True, to='subscribers.Issue', null=True)),
            ],
            options={
                'ordering': ['-created', 'contact__last_name'],
            },
        ),
        migrations.CreateModel(
            name='MailingLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('note', models.CharField(default=b'', help_text=b'Additional info identifying this mailing log.', max_length=50, blank=True)),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('mailings', models.ManyToManyField(to='subscribers.Mailing')),
            ],
        ),
        migrations.CreateModel(
            name='MailingStay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('end_date', models.DateTimeField(help_text=b'Leave blank to withold mail until further notice.', null=True, blank=True)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-end_date'],
            },
        ),
        migrations.CreateModel(
            name='MailingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('modified', models.DateTimeField()),
                ('author', models.ForeignKey(related_name='notes_authored', to=settings.AUTH_USER_MODEL)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='OrganizationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='OtherPurchase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.IntegerField(help_text=b'How many items were purchased?', null=True, blank=True)),
                ('total_cost', models.DecimalField(default=0, max_digits=10, decimal_places=2)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.CreateModel(
            name='OtherPurchaseType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=255)),
            ],
            options={
                'ordering': ['type'],
            },
        ),
        migrations.CreateModel(
            name='Prison',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of this institution', max_length=50)),
                ('subject_of_litigation', models.BooleanField(default=False)),
                ('admin_name', models.CharField(help_text=b"Name of institution administering this prison (e.g. 'Salinas Valley State Prison' administers 'Salinas Valley State Prison -- A'.", max_length=50, blank=True)),
                ('admin_phone', models.CharField(max_length=20, blank=True)),
                ('warden', models.CharField(max_length=50, blank=True)),
                ('men', models.NullBooleanField()),
                ('women', models.NullBooleanField()),
                ('juveniles', models.NullBooleanField()),
                ('minimum', models.NullBooleanField()),
                ('medium', models.NullBooleanField()),
                ('maximum', models.NullBooleanField()),
                ('control_unit', models.NullBooleanField()),
                ('death_row', models.NullBooleanField()),
                ('address', models.ForeignKey(help_text=b'Mailing address for people housed here.', to='subscribers.Address')),
                ('admin_address', models.ForeignKey(related_name='prison_admin_set', blank=True, to='subscribers.Address', help_text=b"Administration's address for this prison, if different from the main address.", null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PrisonAdminType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Name of organization that administers prisons -- e.g. CCA, BOP', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='PrisonType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Type of prison -- e.g. prison, jail, halfway house', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='SharingBatch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('contacts', models.ManyToManyField(to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='SharingPartner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=20, choices=[(b'subscription', b'Subscription'), (b'trial', b'Trial')])),
                ('paid_amount', models.IntegerField(default=0)),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now, db_index=True)),
                ('end_date', models.DateTimeField(help_text=b"Historical field -- don't change", null=True, db_index=True, blank=True)),
                ('number_of_issues', models.IntegerField(default=0)),
                ('cancelled_issues', models.IntegerField(default=0)),
                ('contact', models.ForeignKey(to='subscribers.Contact')),
                ('payer', models.ForeignKey(related_name='subscriptions_payed_for', to='subscribers.Contact')),
            ],
            options={
                'ordering': ['-end_date', '-start_date'],
            },
        ),
        migrations.CreateModel(
            name='SubscriptionSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='SubscriptionStopReason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reason', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=25)),
            ],
        ),
        migrations.AddField(
            model_name='subscription',
            name='source',
            field=models.ForeignKey(blank=True, to='subscribers.SubscriptionSource', help_text=b'Leave blank if unknown.', null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='stop_reason',
            field=models.ForeignKey(blank=True, to='subscribers.SubscriptionStopReason', null=True),
        ),
        migrations.AddField(
            model_name='sharingbatch',
            name='partner',
            field=models.ForeignKey(to='subscribers.SharingPartner'),
        ),
        migrations.AddField(
            model_name='prison',
            name='admin_type',
            field=models.ForeignKey(blank=True, to='subscribers.PrisonAdminType', help_text=b'Organization administering this prison (e.g. CCA, BOP).', null=True),
        ),
        migrations.AddField(
            model_name='prison',
            name='type',
            field=models.ForeignKey(blank=True, to='subscribers.PrisonType', help_text=b'The type of institution -- e.g. prison, jail, halfway house', null=True),
        ),
        migrations.AddField(
            model_name='otherpurchase',
            name='type',
            field=models.ForeignKey(to='subscribers.OtherPurchaseType'),
        ),
        migrations.AddField(
            model_name='mailinglog',
            name='type',
            field=models.ForeignKey(to='subscribers.MailingType'),
        ),
        migrations.AddField(
            model_name='mailinglog',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mailing',
            name='type',
            field=models.ForeignKey(blank=True, to='subscribers.MailingType', null=True),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='request_type',
            field=models.ForeignKey(to='subscribers.InquiryType'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='response_type',
            field=models.ForeignKey(to='subscribers.InquiryResponseType'),
        ),
        migrations.AddField(
            model_name='contactsource',
            name='category',
            field=models.ForeignKey(to='subscribers.ContactSourceCategory', null=True),
        ),
        migrations.AddField(
            model_name='contactaddress',
            name='prison',
            field=models.ForeignKey(blank=True, to='subscribers.Prison', null=True),
        ),
        migrations.AddField(
            model_name='contact',
            name='organization_type',
            field=models.ForeignKey(blank=True, to='subscribers.OrganizationType', help_text=b'Leave blank for individuals.', null=True),
        ),
        migrations.AddField(
            model_name='contact',
            name='source',
            field=models.ForeignKey(blank=True, to='subscribers.ContactSource', help_text=b'How did we find out about this contact? Leave blank if unknown.', null=True),
        ),
        migrations.AddField(
            model_name='contact',
            name='tags',
            field=models.ManyToManyField(help_text=b'List of tags', to='subscribers.Tag', blank=True),
        ),
    ]
