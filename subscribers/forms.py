import re
import json
import datetime
from django import forms
from django.db.models import Q, Max
from django.utils.timezone import now, utc
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.db import transaction

from localflavor.us.forms import USPSSelect

from subscribers.models import *


#
# Utilities
#
def null_model_choices(Model, null_label='[Unknown]'):
    """
    We want model choice fields that have both a blank ("unconstrained"), and
    null ("value is null in database"). So we use a ChoiceField instead of
    ModelChoiceField, and append two different blank values, "---------" and
    "Unknown". This function returns the choices for a Model.
    """
    def lazy():
        return [
            ('', '----------'),
            ('None', null_label)
        ] + [(m.id, str(m)) for m in Model.objects.all()]
    return lazy


class BlankNullBooleanSelect(forms.Select):
    """
    This is copied from NullBooleanSelect, and altered to change the Null value
    from "Unknown" to "-------". NullBooleanSelect doesn't allow overriding
    choices, so we have to copy the whole class. Semantically, we're using
    NullBooleanField's as ("unconstrained", True, False), so "Unknown" is
    confusing.
    """
    def __init__(self, attrs=None):
        choices = (("1", "------"), ("2", "Yes"), ("3", "No"))
        super(BlankNullBooleanSelect, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None):
        try:
            value = {True: '2', False: '3', '2': '2', '3': '3'}[value]
        except KeyError:
            value = '1'
        return super(BlankNullBooleanSelect, self).render(name, value, attrs=attrs)

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        return {'2': True,
                True: True,
                'True': True,
                '3': False,
                'False': False,
                False: False}.get(value, None)


class BlankNullBooleanField(forms.NullBooleanField):
    widget = BlankNullBooleanSelect

    def _has_changed(self, initial, data):
        # For a NullBooleanSelect, None (unknown) and False (No)
        # are not the same
        if initial is not None:
            initial = bool(initial)
        if data is not None:
            data = bool(data)
        return initial != data


class ModelSelectWithAdminLink(forms.Select):
    def __init__(self, admin_url_name=None):
        self.admin_url_name = admin_url_name
        super(ModelSelectWithAdminLink, self).__init__()

    def render(self, *args, **kwargs):
        return mark_safe("%s <a href='%s'>Edit choices</a>" % (
            super(ModelSelectWithAdminLink, self).render(*args, **kwargs),
            reverse(self.admin_url_name)
        ))

#
# Forms
#

class USPSSelectWithEmpty(USPSSelect):
    def __init__(self, *args, **kwargs):
        super(USPSSelectWithEmpty, self).__init__(*args, **kwargs)
        self.choices.insert(0, ('', '-------'))

class ContactSearch(forms.Form):
    # Hidden fields for other searches.
    contact_id = forms.CharField(widget=forms.HiddenInput, required=False)
    donor_batch_id = forms.CharField(widget=forms.HiddenInput, required=False)

    # Name / details
    name = forms.CharField(required=False)
    first_name = forms.CharField(required=False)
    middle_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    account_number = forms.CharField(required=False)
    prisoner_number = forms.CharField(required=False)
    email = forms.CharField(required=False)
    has_been_censored = BlankNullBooleanField(required=False)
    source = forms.ChoiceField(null_model_choices(ContactSource), required=False)
    source_category = forms.ChoiceField(null_model_choices(ContactSourceCategory),
            required=False)
    deceased = BlankNullBooleanField(required=False)

    # Address
    search_addresses_current_on_date = forms.CharField(required=False)
    state = forms.CharField(widget=USPSSelectWithEmpty(), required=False)
    zip = forms.CharField(max_length=10, required=False)
    contact_type = forms.ChoiceField(
            choices=[('', '--------')] + list(
                Contact._meta.get_field('type').choices
            ),
            required=False)
    prison = forms.ModelChoiceField(
            queryset=Prison.objects.all(),
            widget=forms.TextInput(),
            required=False)
    prison_type = forms.ChoiceField(
            null_model_choices(PrisonType), required=False)
    prison_administrator = forms.ChoiceField(
            null_model_choices(PrisonAdminType), required=False)
    subject_of_litigation = BlankNullBooleanField(required=False)
    monitoring = BlankNullBooleanField(required=False)
    investigating = BlankNullBooleanField(required=False)
    lost_contact = BlankNullBooleanField(required=False)

    # Subscription
    is_donor = BlankNullBooleanField(required=False)
    currently_subscribing = BlankNullBooleanField(required=False)
    with_perpetual_subscriptions = BlankNullBooleanField(required=False)
    subscription_source = forms.ChoiceField(null_model_choices(SubscriptionSource),
            required=False)
    tag_search = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
            required=False)

    sort = forms.ChoiceField((
        ("join_date", "Date Joined"),
        ("last_name", "Last Name"),
        ("issues_remaining", "Issues Remaining"),
    ), required=False)

    exclude_missing_names = forms.BooleanField(required=False)
    exclude_addressless = forms.BooleanField(required=False)

    def constrain(self, contacts):
        if not self.is_valid():
            raise Exception("Constraining on invalid form.")
        cd = self.cleaned_data
        # Hidden fields
        if cd['contact_id'] != '':
            contacts = contacts.filter(pk=cd['contact_id'])
        if cd['donor_batch_id'] != '':
            contacts = contacts.filter(donorbatch__id=cd['donor_batch_id'])

        # Name and details
        if cd['name'] != '':
            words = [a.strip() for a in cd['name'].split(' ')]
            for word in words:
                contacts = contacts.filter(
                        Q(first_name__icontains=word) |
                        Q(middle_name__icontains=word) |
                        Q(last_name__icontains=word) |
                        Q(organization_name__icontains=word) |
                        Q(email__icontains=word))
        if cd['first_name'] != '':
            contacts = contacts.filter(first_name__icontains=cd['first_name'])
        if cd['middle_name'] != '':
            contacts = contacts.filter(middle_name__icontains=cd['middle_name'])
        if cd['last_name'] != '':
            contacts = contacts.filter(last_name__icontains=cd['last_name'])
        if cd['account_number'] != '':
            contacts = contacts.filter(id__contains=cd['account_number'])
        if cd['prisoner_number'] != '':
            contacts = contacts.filter(
                    contactaddress__prisoner_number__icontains=cd['prisoner_number'])
        if cd['email'] != '':
            contacts = contacts.filter(email__icontains=cd['email'])
        if cd['has_been_censored'] is not None:
            if cd['has_been_censored']:
                contacts = contacts.filter(mailing__censored=True)
            else:
                contacts = contacts.exclude(mailing__censored=True)
        if cd['contact_type'] != '':
            contacts = contacts.filter(type=cd['contact_type'])
        if cd['source'] != '':
            if cd['source'] == "None":
                contacts = contacts.filter(source__isnull=True)
            else:
                contacts = contacts.filter(source_id=cd['source'])
        if cd['source_category'] != '':
            if cd['source_category'] == "None":
                contacts = contacts.filter(source__category__isnull=True)
            else:
                contacts = contacts.filter(source__category_id=cd['source_category'])
        if cd['deceased'] is not None:
            contacts = contacts.filter(deceased=cd['deceased'])

        # Addresses
        address_filter = Q()
        if cd['state'] != '':
            address_filter &= Q(contactaddress__address__state=cd['state'])
        if cd['zip'] != '':
            zipcode = cd['zip'].strip()
            address_filter &= Q(contactaddress__address__zip__icontains=zipcode)
        if cd['prison'] is not None:
            address_filter &= Q(contactaddress__prison=cd['prison'])
        if cd['prison_type'] != '':
            if cd['prison_type'] == 'None':
                address_filter &= Q(contactaddress__prison__isnull=False,
                        contactaddress__prison__type__isnull=True)
            else:
                address_filter &= Q(contactaddress__prison__type_id=cd['prison_type'])
        if cd['prison_administrator'] != '':
            if cd['prison_administrator'] == 'None':
                address_filter &= Q(contactaddress__prison__isnull=False,
                        contactaddress__prison__admin_type__isnull=True)
            else:
                address_filter &= Q(contactaddress__prison__admin_type_id=cd['prison_administrator'])

        if address_filter and cd['search_addresses_current_on_date'] != '':
            date = cd['search_addresses_current_on_date']
            if re.match('\d{4}-\d\d-\d\d', date):
                address_filter &= Q(contactaddress__start_date__lte=date) & (
                            Q(contactaddress__end_date__isnull=True) |
                            Q(contactaddress__end_date__gte=date)
                        )
        contacts = contacts.filter(address_filter)
        if cd['lost_contact'] is not None:
            contacts = contacts.filter(lost_contact=cd['lost_contact'])
        if cd['subject_of_litigation'] is not None:
            contacts = contacts.filter(contactaddress__prison__subject_of_litigation=cd['subject_of_litigation'])
        if cd['monitoring'] is not None:
            contacts = contacts.filter(contactaddress__prison__monitoring=cd['monitoring'])
        if cd['investigating'] is not None:
            contacts = contacts.filter(contactaddress__prison__investigating=cd['investigating'])


        # Subscription
        if cd['currently_subscribing'] is not None:
            if cd['currently_subscribing']:
                contacts = contacts.exclude(issues_remaining=0)
            else:
                contacts = contacts.filter(issues_remaining=0)

        if cd['is_donor'] is not None:
            if cd['is_donor']:
                contacts = contacts.filter(donor=True)
            else:
                contacts = contacts.filter(donor=False)
        if cd['subscription_source'] != '':
            if cd['subscription_source'] == "None":
                contacts = contacts.filter(subscription__source__isnull=True)
            else:
                contacts = contacts.filter(
                        subscription__source_id=cd['subscription_source'])
        if cd['exclude_addressless']:
            pass
            #contacts = contacts.exclude(contactaddress__isnull=True)
        if cd.get('exclude_missing_names', False):
            pass
            #contacts = contacts.exclude(
            #    first_name='', middle_name='', last_name='', organization_name='')
        if cd['with_perpetual_subscriptions'] is not None:
            if cd['with_perpetual_subscriptions']:
                contacts = contacts.filter(issues_remaining=-1)
            else:
                contacts = contacts.exclude(issues_remaining=-1)

        if cd['tag_search']:
            tag_q = Q()
            for tag in cd['tag_search']:
                tag_q = tag_q | Q(tags=tag)
            contacts = contacts.filter(tag_q)

        contacts = contacts.distinct()

        if cd.get('sort', 'last_name') == 'last_name':
            contacts = contacts.order_by("last_name", "organization_name")
        elif cd['sort'] == "join_date":
            contacts = contacts.order_by("-created")
        elif cd['sort'] == "issues_remaining":
            contacts = contacts.order_by("-issues_remaining")
        return contacts

class ContactAddForm(forms.ModelForm):
    source = forms.ModelChoiceField(
            queryset=ContactSource.objects.all(),
            widget=ModelSelectWithAdminLink("admin:subscribers_contactsource_changelist")
    )
    tag_list = forms.CharField(required=False,
            help_text="Separate with commas. Use existing tags, or type new ones to auto-add them. <a href='/admin/subscribers/tag/'>administer tags</a>")
    def __init__(self, *args, **kwargs):
        super(ContactAddForm, self).__init__(*args, **kwargs)
        self.fields['tag_list'].widget = forms.TextInput(
            attrs={
                'data-available-tags': json.dumps(
                    list(Tag.objects.all().values_list('name', flat=True))
                )
            }
        )
        if 'instance' in kwargs:
            self.fields['tag_list'].initial = kwargs['instance'].tag_list()

    def save(self, *args, **kwargs):
        contact = super(ContactAddForm, self).save(*args, **kwargs)
        contact.set_tag_list(self.cleaned_data['tag_list'])
        return contact

    class Meta:
        model = Contact
        exclude = ['created', 'issues_remaining', 'last_address_date',
                   'issue_adjustment', 'tags']

class ContactAddressForm(forms.ModelForm):
    prison = forms.ModelChoiceField(
        queryset=Prison.objects.all(),
        widget=forms.TextInput,
        required=True
    )

    def create_for(self, contact):
        return ContactAddress.objects.create(
            contact=contact,
            prisoner_number=self.cleaned_data['prisoner_number'].lstrip('#'),
            unit=self.cleaned_data['unit'],
            prison=self.cleaned_data['prison'],
            control_unit=self.cleaned_data['control_unit'],
            death_row=self.cleaned_data['death_row'],
        )

    class Meta:
        model = ContactAddress
        exclude = ['address', 'contact', 'start_date', 'end_date']

class AddressForm(forms.ModelForm):

    class Meta:
        fields = '__all__'
        model = Address

class SubscriptionForm(forms.Form):
    type = forms.ChoiceField(choices=(('subscription', 'Subscription'), ('trial', 'Trial')))
    source = forms.ModelChoiceField(
        queryset=SubscriptionSource.objects.all(),
        widget=ModelSelectWithAdminLink("admin:subscribers_subscriptionsource_changelist")
    )
    number_of_months = forms.IntegerField(help_text="e.g. 1, 3, 12, 24.<br />Enter &ldquo;-1&rdquo; for a perpetual subscription.", label="Number of issues")
    payer = forms.ModelChoiceField(
        widget=forms.TextInput,
        queryset=Contact.objects.all(),
        help_text="Leave blank if self-paying",
        required=False
    )

    def clean_number_of_months(self):
        if self.cleaned_data['number_of_months'] == 0 or \
                self.cleaned_data['number_of_months'] < -1:
            raise ValidationError("Invalid number of months.")
        return self.cleaned_data['number_of_months']

    def create_for(self, contact):
        num_months = self.cleaned_data['number_of_months']
        start_date = now()
        return Subscription.objects.create(
            number_of_issues=num_months,
            payer=self.cleaned_data['payer'] or contact,
            source=self.cleaned_data['source'],
            contact=contact,
            start_date=start_date,
            type=self.cleaned_data['type'],
        )

class OtherPurchaseForm(forms.ModelForm):
    type = forms.ModelChoiceField(
        queryset=OtherPurchaseType.objects.all(),
        widget=ModelSelectWithAdminLink("admin:subscribers_otherpurchasetype_changelist")
    )

    class Meta:
        model = OtherPurchase
        fields = ['type', 'count', 'total_cost', 'date']

class StopReasonForm(forms.Form):
    reason = forms.ModelChoiceField(
            queryset=SubscriptionStopReason.objects.all(),
            widget=ModelSelectWithAdminLink(
                "admin:subscribers_subscriptionstopreason_changelist"),
    )

class IssueForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Issue

class SharingBatchForm(forms.ModelForm):
    partner = forms.ModelChoiceField(
        queryset=SharingPartner.objects.all(),
        widget=ModelSelectWithAdminLink("admin:subscribers_sharingpartner_changelist")
    )
    class Meta:
        model = SharingBatch
        exclude = ['contacts', 'date']

class InquiryForm(forms.ModelForm):
    request_type = forms.ModelChoiceField(
        queryset=InquiryType.objects.order_by('description'),
        widget=ModelSelectWithAdminLink("admin:subscribers_inquirytype_changelist")
    )
    response_type = forms.ModelChoiceField(
        queryset=InquiryResponseType.objects.order_by('description'),
        widget=ModelSelectWithAdminLink("admin:subscribers_inquiryresponsetype_changelist")
    )
    class Meta:
        model = Inquiry
        exclude = ['date', 'batch', 'contact']

class MailingStayForm(forms.ModelForm):
    class Meta:
        model = MailingStay
        exclude = ['contact']

class MailingForm(forms.ModelForm):
    class Meta:
        model = Mailing
        exclude = ['contact', 'created', 'inquiry', 'censored',
                   'notes', 'sent', 'custom_text']

class MailingTypeForm(forms.Form):
    mailing_type = forms.ModelChoiceField(
        queryset=MailingType.objects.all(),
        widget=ModelSelectWithAdminLink("admin:subscribers_mailingtype_changelist")
    )
    note = forms.CharField(widget=forms.Textarea,
            help_text="Optional note explaining this mailing (for record keeping only)",
            required=False)


class InstitutionLabelForm(forms.Form):
    label_prefix = forms.CharField(help_text="e.g. ATTN: Law Library", required=False)

class ConfirmPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ConfirmPasswordForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        pw = self.cleaned_data.get('password')
        if pw and not self.user.check_password(pw):
            raise ValidationError("Password not recognized.")
    
