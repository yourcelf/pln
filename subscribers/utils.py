import xlsxwriter
import datetime
from io import BytesIO
from django.utils.timezone import utc
from django.http import HttpResponse
from django.conf import settings
from pytz import timezone
from reportlab.pdfgen import canvas

from localflavor.us.us_states import USPS_CHOICES

STATE_NAMES = dict(USPS_CHOICES)

SPREADSHEET_FORMATS = {}
SPREADSHEET_FORMATS['default'] = [
        ("Subscriber #", lambda c: str(c['contact'].pk)),
        ("Issues Remaining", lambda c: c['contact'].get_issues_remaining_string()),
        ("First or Org name",
            lambda c: c['contact'].organization_name or c['contact'].first_name),
        ("Last name", lambda c: c['contact'].last_name),
        ("DOC number", lambda c: c['cad'].prisoner_number),
        ("Unit", lambda c: c['cad'].unit or ""),
        ("Prison", lambda c: c['cad'].prison.name if c['cad'].prison else ""),
        ("Address1", lambda c: c['add'].address1),
        ("Address2", lambda c: c['add'].address2),
        ("City", lambda c: c['add'].city),
        ("State", lambda c: c['add'].state),
        ("Zip", lambda c: c['add'].zip),
        ("Country", lambda c: c['add'].country or "USA"),
        ("Organization", lambda c: c['contact'].organization_name or ""),
        ("Full name", lambda c: " ".join(
            (c['contact'].first_name, c['contact'].last_name)
        ).strip()),
]
SPREADSHEET_FORMATS['inquiry'] = SPREADSHEET_FORMATS['default'][:] + [
        ("Inquiry code", lambda c: c['inquiry'].response_type.code),
]
SPREADSHEET_FORMATS['blackstone'] = [
        ("First", lambda c: c['contact'].organization_name or c['contact'].first_name),
        ("Last", lambda c: c['contact'].last_name),
        ("DOCNumber", lambda c: c['cad'].prisoner_number),
        ("Prison", lambda c: c['cad'].prison.name if c['cad'].prison else ""),
        ("Address", lambda c: " ".join([
            a for a in (c['add'].address1, c['add'].address2) if a
        ])),
        ("City", lambda c: c['add'].city),
        ("StateName", lambda c: STATE_NAMES.get(c['add'].state, c['add'].state)),
        ("Zip", lambda c: c['add'].zip),
]


def count_months(d1, d2):
    return (d2.year - d1.year) * 12 + d2.month - d1.month

def _bulk_contact_addresses(objects):
    from subscribers.models import Inquiry, Contact, ContactAddress, Address
    contact_ids = set()
    for obj in objects:
        if isinstance(obj, Inquiry):
            contact_ids.add(obj.contact_id)
        elif isinstance(obj, Contact):
            contact_ids.add(obj.id)

    cads = {}
    for cad in ContactAddress.objects.select_related(
                'address', 'prison', 'contact'
            ).filter(contact_id__in=contact_ids, end_date__isnull=True):
        cads[cad.contact_id] = cad

    rows = []
    for obj in objects:
        row = {}
        if isinstance(obj, Inquiry):
            row['inquiry'] = obj
            row['contact'] = obj.contact
        elif isinstance(obj, Contact):
            row['contact'] = obj
        else:
            row['other'] = obj
        if 'contact' in row:
            row['cad'] = cads.get(row['contact'].pk,
                    ContactAddress(contact=row['contact'], address=Address()))
            row['add'] = row['cad'].address
        rows.append(row)
    return rows

def _write_xlsx(header, rows):
    fh = BytesIO()
    workbook = xlsxwriter.Workbook(fh)
    worksheet = workbook.add_worksheet()
    bold = workbook.add_format({'bold': 1})

    col_widths = [0 for h in header]
    for col, title in enumerate(header):
        worksheet.write_string(0, col, str(title), bold)
        col_widths[col] = max(col_widths[col], len(title))

    for row, rowdata in enumerate(rows):
        for col, cell in enumerate(rowdata):
            worksheet.write_string(row+1, col, str(cell))
            col_widths[col] = max(col_widths[col], len(cell))

    for col, width in enumerate(col_widths):
        worksheet.set_column(col, col, width)
    workbook.close()
    fh.seek(0)
    return fh.getvalue()


def institution_xlsx(institutions):
    header =  ["name", "address1", "address2", "city", "state", "zip"]
    rows = []
    for prison in institutions:
        rows.append([
            prison.name,
            prison.address.address1,
            prison.address.address2,
            prison.address.city,
            prison.address.state,
            prison.address.zip,
        ])
    return _write_xlsx(header, rows)

def institution_xlsx_response(institutions):
    response = HttpResponse(institution_xlsx(institutions))
    response['Content-type'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8"
    response['Content-disposition'] = "attachment; filename=institutions.xlsx"
    return response

def contact_xlsx(objects, fmt="default"):
    col_format = SPREADSHEET_FORMATS[fmt]
    addresses = _bulk_contact_addresses(objects)

    header = [title for title, formatter in col_format]
    rows = []
    for address in addresses:
        row = []
        for title, formatter in col_format:
            row.append(formatter(address))
        rows.append(row)
    return _write_xlsx(header, rows)

def contact_xlsx_response(contacts, fmt="default"):
    response = HttpResponse(contact_xlsx(contacts, fmt))
    response['Content-type'] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8"
    response['Content-disposition'] = "attachment; filename=contacts.xlsx"
    return response

def contact_pdf_response(objects, subdetails=False):
    # Print mailing labels, appropriate for a Dymo printer.
    out = BytesIO()
    width = 3.5 * 72. # convert from inches to points (1/72 inch).
    height = 1.125 * 72. # convert from inches to points (1/72 inch).
    c = canvas.Canvas(out, pagesize=(width, height))

    rows = _bulk_contact_addresses(objects)

    for row in rows:
        c.setFont("Helvetica", 9)
        lineno = 0
        if subdetails and 'contact' in row:
            contact = row['contact']
            c.setFont("Helvetica", 8)
            details = "Subscriber #%s" % contact.id
            if contact.issues_remaining > 0:
                details += " (%s left)" % contact.issues_remaining
            c.drawRightString(width - 10, height - height / 7. * (lineno + 1), details)
            c.setFont("Helvetica", 9)
            lineno += 1

        if 'cad' in row:
            address_lines = row['cad'].get_address_lines()
        elif 'other' in row:
            address_lines = row['other']
        else:
            address_lines = []

        for line in address_lines:
            c.drawString(10, height - height / 7. * (lineno + 1), line)
            lineno += 1
        if 'inquiry' in row:
            c.drawString(180, height - height / 7. * 5.5,
                         row['inquiry'].response_type.code)
        c.showPage()
    c.save()
    response = HttpResponse(out.getvalue())
    response['Content-type'] = "application/pdf"
    response['Content-disposition'] = "attachment; filename=labels.pdf"
    return response

