from django import template

register = template.Library()

@register.filter
def mailings_path(path, issue):
    if not path:
        return ""
    what = []
    if path.endswith("needed"):
        what.append("Needed")
    elif path.endswith("enqueued"):
        what.append("Enqueued")
    elif path.endswith("sent"):
         what.append("Sent")
    elif path.endswith("unreachable"):
         what.append("Unreachable")
    if path.startswith("notices"):
        if "recipient" in path:
             what.append("recipients with ")
        else:
             what.append("payers with ")
        if "0" in path:
             what.append(" 0 issues left")
        elif "1" in path:
             what.append(" 1 issues left")
        elif "2" in path:
             what.append(" 2 issues left")
    elif path.startswith("issues:"):
         what.append(" recipients of ")
         what.append(str(issue))
    elif path.startswith("inquiries"):
         what.append(" inquiries")
    what = "".join(what)
    return what

